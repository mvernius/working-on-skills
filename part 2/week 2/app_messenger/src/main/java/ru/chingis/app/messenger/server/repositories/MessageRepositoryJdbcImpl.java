package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryJdbcImpl implements MessageRepository {
    private final Connection CONNECTION;

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from message where id = (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from message";
    //language=SQL
    private static final String SQL_INSERT = "insert into message (sentence, id_user, id_room) values (?, ?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from message where id = (?)";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update message set sentence = (?) where id = (?)";


    public MessageRepositoryJdbcImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private final RowMapper<Message> messagesRowMapper = row -> new Message(
            row.getInt("id"),
            row.getString("message_text"),
            row.getInt("id_user"),
            row.getInt("id_room")
    );

    @Override
    public void save(Message object) {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SQL_INSERT)) {
            preparedStatement.setString(1, object.getText());
            preparedStatement.setInt(2, object.getId_user());
            preparedStatement.setInt(3, object.getId_room());
            int check = preparedStatement.executeUpdate();
            if (check > 0)
                System.out.println("Message was saved");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID + id)) {
            int check = statement.executeUpdate();
            if (check > 0)
                System.out.println("Message was deleted");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE_BY_ID)) {
            statement.setString(1, object.getText());
            statement.setInt(2, object.getId());
            int check = statement.executeUpdate();
            if (check > 0) {
                System.out.println("Message was updated");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    // идея динамического поиска как в мессенджерах. Подсветка слова и тд. Поиск подстроки в строке.
    // когда-нибудь здесь будет реализован кмп
    @Override
    public List<Message> findStringOccurance(String text) {
        return null;
    }


    //еще одна реализация сверхразума
    @Override
    public Message find(Integer id) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ID);
            try {
                statement.setInt(1, id);
                statement.execute();
                try (ResultSet resultSet = statement.getResultSet()) {
                    resultSet.next();
                    return messagesRowMapper.mapRow(resultSet);
                } catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            } finally {
                statement.close();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Message> findAll() {
        try (Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<Message> list = new ArrayList<>();
            while (resultSet.next()) {
                Message message = messagesRowMapper.mapRow(resultSet);
                list.add(message);
            }
            return list;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
