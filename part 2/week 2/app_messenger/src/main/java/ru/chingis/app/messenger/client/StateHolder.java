package ru.chingis.app.messenger.client;

public class StateHolder {
    private volatile ClientState clientState = ClientState.CONNECTED;

    public ClientState getClientState() {
        return clientState;
    }

    public void setClientState(ClientState clientState) {
        this.clientState = clientState;
    }
}
