package ru.chingis.app.messenger.client;

import ru.chingis.app.messenger.server.command.MessageCommand;

import java.io.*;
import java.net.Socket;

class ListenServer extends Thread {
    private final Socket socket;

    ListenServer(Socket socket) {
        this.socket = socket;
        try {
            this.fromServerObject = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Object message = null;
                try {
                    message = fromServerObject.readObject();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                if (message == null) {
                    break;
                }
                if (message instanceof MessageCommand) {
                    MessageCommand messageCommand = (MessageCommand) message;
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                fromServerObject.close();
            } catch (IOException ignored) {
            }
        }
    }
}
