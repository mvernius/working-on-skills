package ru.chingis.app.messenger.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServerImpl implements Server {
    private ServerSocket serverSocket;
    private final List<ServerClientConnectionProcessor> listOfExecutables = Collections.synchronizedList(new ArrayList<>());
    private final int port;

    public ServerImpl(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server successfully launched...");
            while (true) {
                Socket socketClient = serverSocket.accept();
                try {
                    listOfExecutables.add(new ServerClientConnectionProcessor(this, socketClient));
                } catch (IOException e) {
                    socketClient.close();
                }
            }
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    public List<ServerClientConnectionProcessor> getListOfExecutables() {
        return listOfExecutables;
    }

    public void shutdown() throws IOException {
        for (Thread thread : listOfExecutables) {
            thread.interrupt();
        }
        if (serverSocket != null) {
            serverSocket.close();
        }
    }
}
