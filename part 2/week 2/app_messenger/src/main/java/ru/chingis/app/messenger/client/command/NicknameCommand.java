package ru.chingis.app.messenger.client.command;

public class NicknameCommand extends ClientToServerCommand {
    private final String nickname;

    public NicknameCommand(String nickname) {
        this.nickname = nickname;
    }
}
