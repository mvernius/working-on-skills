package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.Room;

public interface RoomRepository extends CrudRepository<Room>{
}
