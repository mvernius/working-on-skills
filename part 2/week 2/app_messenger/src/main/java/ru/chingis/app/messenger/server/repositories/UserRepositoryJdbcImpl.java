package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryJdbcImpl implements UserRepository {
    private final Connection CONNECTION;
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from app_user where id = (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from app_user";
    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from app_user where nickname = (?)";
    //language=SQL
    private static final String SQL_INSERT = "insert into app_user (id, nickname) values (?, ?)"; //curios if I need id here..
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from app_user where id = (?)";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update app_user set nickname = (?) where id = (?)";

    private RowMapper<User> userRowMapper = row -> new User(
            row.getInt("id"),
            row.getString("nickname")
    );


    public UserRepositoryJdbcImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    @Override
    public void save(User object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getNickname());
            int check = statement.executeUpdate();
            if (check > 0)
                System.out.println("User was created");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setInt(1, id);
            int check = statement.executeUpdate();
            if (check > 0)
                System.out.println("User was deleted");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE_BY_ID)) {
            statement.setString(1, object.getNickname());
            statement.setInt(2, object.getId());
            int check = statement.executeUpdate();
            if (check > 0)
                System.out.println("User was updated");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //need to check, new method for resultSet and prepared statement in find
    @Override
    public User find(Integer id) {
        ResultSet resultSet = null;
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
            resultSet = statement.getResultSet();
            if (resultSet.next())
                return userRowMapper.mapRow(resultSet);
            else
                return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
        }
    }

    @Override
    public User findByNickname(String nickname) {
        ResultSet resultSet = null;
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            statement.execute();
            resultSet = statement.getResultSet();
            if (resultSet.next())
                return userRowMapper.mapRow(resultSet);
            else
                return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
        }
    }

    @Override
    public List<User> findAll() {
        try (Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<User> resultList = new ArrayList<>();
            while (resultSet.next()) {
                resultList.add(userRowMapper.mapRow(resultSet));
            }
            return resultList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
