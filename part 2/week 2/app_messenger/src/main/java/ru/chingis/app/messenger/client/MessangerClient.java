package ru.chingis.app.messenger.client;

import ru.chingis.app.messenger.client.command.ClientToServerCommand;
import ru.chingis.app.messenger.client.command.NicknameCommand;

import java.io.*;
import java.net.Socket;
// все что так закомменчено, не уверен в необходимости
public class MessangerClient implements Runnable {
    private Socket socketclient;
    private BufferedReader reader;
    private ListenServer listenServer;
    private ObjectOutputStream toServerObject;
    private StateHolder stateHolder;

    public MessangerClient(String host, int port) {
        try {
            this.socketclient = new Socket(host, port);
            this.reader = new BufferedReader(new InputStreamReader(System.in));
            this.toServerObject = new ObjectOutputStream(new BufferedOutputStream(socketclient.getOutputStream()));
            stateHolder = new StateHolder();
            this.listenServer.start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void run() {
        try {
            initNewUser();
            while (true) {
                String clientsMessage = reader.readLine();
                ClientToServerCommand command = CommandDetector.detect(clientsMessage);
                sendMessage(command);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean sendMessage(ClientToServerCommand command) {
        try {
            toServerObject.writeObject(command);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void initNewUser() throws IOException {
        System.out.print("Insert your nickname: ");
        String nickname = reader.readLine();
        NicknameCommand nicknameCommand = new NicknameCommand(nickname);
        if (sendMessage(nicknameCommand)) {
            stateHolder.setClientState(ClientState.USERNAME_IS_SET);
        } else {
            System.err.println("Sorry, something went wrong");
        }
    }

/*
    public void setUserName(String userName) {
        String util = "Server//nickname//";
        toServer.println(util + userName);
    }

    public void showAvailableRooms() {
        String util = "Server//show rooms//";
        toServer.println(util);
    }

    public void chooseRoom(String room) {
        String util = "Server//choose room//";
        toServer.println(util + room);
    }

    public void exitRoom() {
        String util = "Server//exit room//";
        toServer.println(util);
    }*/

}
