package ru.chingis.app.messenger.server;



import java.sql.Connection;
import java.sql.DriverManager;

public class MainRepoTest {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/messenger";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        UserRepository userRepository = new UserRepositoryJdbcImpl(connection);
        System.out.println(userRepository.find(2));
        System.out.println(userRepository.findByNickname("tester"));
        System.out.println(userRepository.findByNickname("abracodabra"));
        RoomRepository roomRepository = new RoomRepositoryJdbcImpl(connection);
        System.out.println(roomRepository.find(1));
        System.out.println(roomRepository.findAll());
        Room room = Room.builder()
                .id(4)
                .roomName("poligon")
                .build();
        roomRepository.save(room);
        room.setRoomName("poligon changed");
        roomRepository.update(room);
    }
}
