package ru.chingis.app.messenger.server;

import ru.chingis.app.messenger.server.service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

class ServerClientConnectionProcessor extends Thread {
    private final ServerImpl server;
    private final BufferedReader income;
    private final PrintWriter outcome;
    private Service service;

    public ServerClientConnectionProcessor(ServerImpl server, Socket socket) throws IOException {
        this.server = server;
        income = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        outcome = new PrintWriter(socket.getOutputStream(), true);
        start();
    }

    @Override
    public void run() {
        String inputLine;
        try {
            while (true) {
                inputLine = income.readLine();
                if (inputLine.equals("stop")) {
                    break;
                }
                for (ServerClientConnectionProcessor toClient : server.getListOfExecutables()) {
                    toClient.send(inputLine);
                }
            }
        } catch (IOException ignored) {
        }
    }

    private void send(String input) {
        outcome.println(Thread.currentThread().getName() + " tells: " + input);
    }
}
