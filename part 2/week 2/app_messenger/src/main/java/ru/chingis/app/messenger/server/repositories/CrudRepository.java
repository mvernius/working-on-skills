package ru.chingis.app.messenger.server.repositories;

import java.util.List;

public interface CrudRepository<T> {
    void save(T object);
    T find(Integer id);
    void delete(Integer id);
    void update(T object);
    List<T> findAll();
}
