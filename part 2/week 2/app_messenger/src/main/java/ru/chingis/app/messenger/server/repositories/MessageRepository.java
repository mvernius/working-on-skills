package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.Message;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message> {
    List<Message> findStringOccurance(String text);
}
