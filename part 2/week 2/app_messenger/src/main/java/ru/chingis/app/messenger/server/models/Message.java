package ru.chingis.app.messenger.server.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Message {
    Integer id;
    String text;
    Integer id_user;
    Integer id_room;
}
