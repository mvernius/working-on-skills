package ru.chingis.app.messenger.client;

public enum ClientState {
    CONNECTED,
    USERNAME_IS_SET,
    IS_IN_ROOM
}
