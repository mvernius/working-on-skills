package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.Room;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoomRepositoryJdbcImpl implements RoomRepository {
    private final Connection CONNECTION;
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from app_room where id = (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from app_room";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "select * from app_room where id = (?)";
    //language=SQL
    private static final String SQL_INSERT = "insert into app_room (id, room_name) values(?, ?)";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update app_room set room_name = (?) where id = (?)";

    public RoomRepositoryJdbcImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private RowMapper<Room> roomRowMapper = row -> new Room(
            row.getInt("id"),
            row.getString("room_name")
    );

    @Override
    public void save(Room object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getRoomName());
            int checker = statement.executeUpdate();
            if (checker > 0)
                System.out.println("Room was successfully created");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setInt(1, id);
            int checker = statement.executeUpdate();
            if (checker > 0)
                System.out.println("Room was successfully deleted");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(Room object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE_BY_ID)) {
            statement.setString(1, object.getRoomName());
            statement.setInt(2, object.getId());
            int checker = statement.executeUpdate();
            if (checker > 0)
                System.out.println("Room was successfully updated");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    //метод сверхразума
    @Override
    public Room find(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                resultSet.next();
                return roomRowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Room> findAll() {
        try (Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<Room> resultList = new ArrayList<>();
            while (resultSet.next()) {
                resultList.add(roomRowMapper.mapRow(resultSet));
            }
            return resultList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
