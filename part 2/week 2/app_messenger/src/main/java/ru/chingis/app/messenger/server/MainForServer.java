package ru.chingis.app.messenger.server;

public class MainForServer {
    public static void main(String[] args) {
        int port = 7777;
        Server server = new ServerImpl(port);
        server.run();
    }
}
