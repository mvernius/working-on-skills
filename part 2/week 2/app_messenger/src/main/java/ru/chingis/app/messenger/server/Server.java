package ru.chingis.app.messenger.server;

import java.io.IOException;

public interface Server extends Runnable {
    void shutdown() throws IOException;
}
