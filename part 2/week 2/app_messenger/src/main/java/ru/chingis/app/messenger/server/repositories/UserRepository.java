package ru.chingis.app.messenger.server.repositories;

import ru.chingis.app.messenger.server.models.User;

public interface UserRepository extends CrudRepository<User>{
    User findByNickname(String nickname);
}
