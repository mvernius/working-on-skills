package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Course;

public interface CoursesRepository extends CrudRepository<Course> {
    Course findByTitle(String title);
}
