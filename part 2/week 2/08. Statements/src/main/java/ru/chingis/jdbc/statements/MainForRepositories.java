package ru.chingis.jdbc.statements;

import ru.chingis.jdbc.statements.models.Course;
import ru.chingis.jdbc.statements.models.Lesson;
import ru.chingis.jdbc.statements.models.Student;
import ru.chingis.jdbc.statements.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

public class MainForRepositories {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(connection);
        Scanner scanner = new Scanner(System.in);
       /* System.out.print("Write last name: ");
        String lastName = scanner.nextLine();
        System.out.print("Write name: ");
        String firstName = scanner.nextLine();
        System.out.print("Write age: ");
        Integer age = scanner.nextInt();
        Student student = Student.builder()
                .lastName(lastName)
                .name(firstName)
                .age(age)
                .isActive(true)
                .build();

        studentsRepository.save(student);
        student.setName("Bobby");
        System.out.println(student);
        studentsRepository.update(student); //update doesn't work
//        studentsRepository.delete(8); works*/

        LessonsRepository lessonsRepository = new LessonsRepositoryJdbcImpl(connection);
        Lesson lesson = new Lesson(null, "test", 2);
        lessonsRepository.save(lesson);
        lesson.setName("test complete");
        lessonsRepository.update(lesson);
    }
}
