package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Course;
import ru.chingis.jdbc.statements.models.Lesson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CoursesRepositoryJdbcImpl implements CoursesRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from course";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from course where id = ";
    //language=SQL
    private static final String SQL_SELECT_FROM_LESSON_BY_COURSE_ID = "select * from lesson where course_id = ";
    //language=SQL
    private static final String SQL_INSERT_ALL = "insert into course (title, start_date, end_date) values (?, ?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from lesson where id = ";
    //language=SQL
    private static final String SQL_SELECT_LAST = "select max(id) from lesson";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update course set title = (?), start_date = (?), end_date = (?) " +
            "where id = ";

    //не понимаю, как проитерировать по этому запросу и вытащить нужную модель lesson (возвращает пустой список)
    //language=SQL
    //private static final String SQL_SELECT_JOIN_LESSON_BY_COURSE_ID = "select * from course c join lesson l on c.id = l.id where c.id = ";

    private final Connection CONNECTION;

    public CoursesRepositoryJdbcImpl(Connection connection) { this.CONNECTION = connection; }

    private final RowMapper<Lesson> lessonsRowMapper = row -> new Lesson(
            row.getInt("id"),
            row.getString("name"),
            row.getInt("course_id")
    );

    private final RowMapper<Course> coursesRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(
                    row.getInt("id"),
                    row.getString("title"),
                    row.getDate("start_date"),
                    row.getDate("end_date")
            );
        }
    };


    @Override
    public void save(Course object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT_ALL);
            Statement updateId = CONNECTION.createStatement();
            ResultSet resultSet = updateId.executeQuery(SQL_SELECT_LAST)) {
            statement.setString(1, object.getTitle());
            statement.setDate(2, object.getStartDate());
            statement.setDate(3, object.getEndDate());
            statement.executeUpdate();
            resultSet.next();
            object.setId(resultSet.getInt("max") + 1);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Course object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE_BY_ID + object.getId())) {
            statement.setString(1, object.getTitle());
            statement.setDate(2, object.getStartDate());
            statement.setDate(3, object.getEndDate());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID + id)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Course findByTitle(String title) {
        return null;
    }

    @Override
    public Course find(Integer id) {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet setForCourse = statement.executeQuery(SQL_SELECT_BY_ID + id);
             ResultSet setForLesson = statement.executeQuery(SQL_SELECT_FROM_LESSON_BY_COURSE_ID + id)){
            List<Lesson> lessons = new ArrayList<>();
            setForCourse.next();
            Course course = coursesRowMapper.mapRow(setForCourse);
            while (setForLesson.next()) {
                Lesson lesson = lessonsRowMapper.mapRow(setForLesson);
                lesson.setCourse(course);
                lessons.add(lesson);
            }
            course.setLessons(lessons);
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Course> findAll() {
        ResultSet setForLesson = null;

        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
            List<Course> result = new ArrayList<>();
            Course course;
            while (resultSet.next()) {
                List<Lesson> lessons = new ArrayList<>();
                course = coursesRowMapper.mapRow(resultSet);
                setForLesson = statement.executeQuery(SQL_SELECT_FROM_LESSON_BY_COURSE_ID + course.getId());
                while (setForLesson.next()) {
                    Lesson lesson = lessonsRowMapper.mapRow(setForLesson);
                    lesson.setCourse(course);
                    lessons.add(lesson);
                }
                course.setLessons(lessons);
                result.add(course);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                if (setForLesson != null) setForLesson.close();
            } catch (SQLException ignored) {}
        }
    }
}
