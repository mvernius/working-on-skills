package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Lesson;

public interface LessonsRepository extends CrudRepository<Lesson> {
    Lesson findByName(String name);
}
