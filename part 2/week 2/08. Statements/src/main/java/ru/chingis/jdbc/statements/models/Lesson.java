package ru.chingis.jdbc.statements.models;


//@NoArgsConstructor
//@Data
public class Lesson {
    private Integer id;
    private String name;
    private Integer courseId;
    private Course course;

    public Lesson(Integer id, String name, Integer courseId) {
        this.id = id;
        this.name = name;
        this.courseId = courseId;
    }

    public Lesson() {}

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public Course getCourse() {
        return course;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        try {
            String string = "Lesson{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", courseId=" + courseId +
                    //", title='" + course.toStringCustom() + '\'' +
                    '}';
            return string;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
