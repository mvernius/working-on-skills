package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
    Student findByName(String name);
}
