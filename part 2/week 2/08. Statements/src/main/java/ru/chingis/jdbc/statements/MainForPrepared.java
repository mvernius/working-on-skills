package ru.chingis.jdbc.statements;

import java.sql.*;
import java.util.Scanner;

public class MainForPrepared {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        Scanner scanner = new Scanner(System.in);
        System.out.print("Type lesson: ");
        String lesson = scanner.nextLine();
        printCurrentPreparedStatements(connection);

        callPrepareStatementForLesson(connection, lesson);
    }

    private static void callPrepareStatementForLesson(Connection connection, String lesson) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("select s.name, s.last_name, c.title\n" +
                "                                        from student s\n" +
                "                                                 inner join course_student cs on s.id = cs.student_id\n" +
                "                                                 inner join course c on c.id = cs.course_id\n" +
                "                                                 inner join lesson l on c.id = l.course_id\n" +
                "                                        where l.name = ?");
        preparedStatement.setString(1, lesson);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    private static void printCurrentPreparedStatements(Connection connection) throws SQLException {
        Statement getPreparedStatements = connection.createStatement();
        ResultSet preparedStatements = getPreparedStatements.executeQuery("select statement from pg_prepared_statements");

        while (preparedStatements.next()) {
            System.out.println(preparedStatements.getString("statement"));
        }
        preparedStatements.close();
        getPreparedStatements.close();
    }
}
