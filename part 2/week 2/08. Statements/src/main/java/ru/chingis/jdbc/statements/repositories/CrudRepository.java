package ru.chingis.jdbc.statements.repositories;

import java.util.List;

/*
репозиторий - интерфейс, слой, разграничивающий бизнес-логику от задач работы с бд.
 */
/*
C - create
R - read
U - update
D - delete
 */
public interface CrudRepository<T> {
    void save(T object);
    T find(Integer id);
    void update(T object);
    void delete(Integer id);
    List<T> findAll();
}
