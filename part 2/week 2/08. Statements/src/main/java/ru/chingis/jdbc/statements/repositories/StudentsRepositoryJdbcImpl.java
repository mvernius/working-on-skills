package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from student";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";
    //language=SQL
    private static final String SQL_INSERT_ALL = "insert into student (name, last_name, age, is_active) values (?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from student where id = ";
    //language=SQL
    private static final String SQL_UPDATE = "update student set name = (?), last_name = (?), age = (?), is_active = (?) where id = (?)";

    private final Connection CONNECTION;

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private RowMapper<Student> studentRowMapper = new RowMapper<Student>() {
        @Override
        public Student mapRow(ResultSet row) throws SQLException {
            return new Student(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("last_name"),
                    row.getInt("age"),
                    row.getBoolean("is_active")
            );
        }
    };

    @Override
    public void save(Student object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT_ALL)){
            statement.setString(1, object.getName());
            statement.setString(2, object.getLastName());
            statement.setInt(3, object.getAge());
            statement.setBoolean(4, object.isActive());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public void update(Student object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, object.getName());
            statement.setString(2, object.getLastName());
            statement.setInt(3, object.getAge());
            statement.setBoolean(4, object.isActive());
            statement.setInt(5, object.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID + id)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Student findByName(String name) {
        return null;
    }

    @Override
    public Student find(Integer id) {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id)) {
            resultSet.next();
            return studentRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Student> findAll() {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)){
            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                Student student = studentRowMapper.mapRow(resultSet);
                result.add(student);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
