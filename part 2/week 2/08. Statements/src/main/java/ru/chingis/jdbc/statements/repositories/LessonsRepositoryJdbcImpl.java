package ru.chingis.jdbc.statements.repositories;

import ru.chingis.jdbc.statements.models.Lesson;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LessonsRepositoryJdbcImpl implements LessonsRepository {
    private final Connection CONNECTION;
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from lesson";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from lesson where id = ";
    //language=SQL
    private static final String SQL_INSERT_ALL = "insert into lesson (name, course_id) values (?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from lesson where id = ";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update lesson " +
            "set name = (?), course_id = (?) " +
            "where id = ";
    //language=SQL
    private static final String SQL_SELECT_LAST = "select max(id) from lesson";

    public LessonsRepositoryJdbcImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private final RowMapper<Lesson> LESSONS_ROW_MAPPER = row -> new Lesson(
            row.getInt("id"),
            row.getString("name"),
            row.getInt("course_id")
    );


    //крутейшая реализация получения id после сохранения объекта в бд (на самом деле костыль)
    @Override
    public void save(Lesson object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT_ALL);
            Statement updateId = CONNECTION.createStatement();
            ResultSet resultSet = updateId.executeQuery(SQL_SELECT_LAST)) {
            statement.setString(1, object.getName());
            statement.setInt(2, object.getCourseId());
            statement.executeUpdate();
            resultSet.next();
            object.setId(resultSet.getInt("max") + 1);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Lesson object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_UPDATE_BY_ID + object.getId())) {
            statement.setString(1, object.getName());
            statement.setInt(2, object.getCourseId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_ID + id)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Lesson find(Integer id) {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id)) {
            resultSet.next();
            Lesson lesson = LESSONS_ROW_MAPPER.mapRow(resultSet);
            lesson.setCourse(new CoursesRepositoryJdbcImpl(CONNECTION).find(lesson.getCourseId()));
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Lesson findByName(String name) {
        return null;
    }

    @Override
    public List<Lesson> findAll() {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)){
            List<Lesson> result = new ArrayList<>();
            while (resultSet.next()) {
                Lesson lesson = LESSONS_ROW_MAPPER.mapRow(resultSet);
                lesson.setCourse(new CoursesRepositoryJdbcImpl(CONNECTION).find(lesson.getCourseId()));
                result.add(lesson);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
