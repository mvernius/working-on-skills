package ru.chingis.jdbc.statements.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Student {
    private Integer id; //а для чего это поле хз, id в бд проставляется само. Можно прописать заполнение id через запрос
    private String name;
    private String lastName;
    private Integer age;
    private boolean isActive;
}
