package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Course;

public interface CoursesRepository extends CrudRepository<Course> {
    Course findByTitle(String title);
}
