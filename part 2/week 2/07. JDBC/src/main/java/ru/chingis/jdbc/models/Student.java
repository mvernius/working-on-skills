package ru.chingis.jdbc.models;
// реализация моделей
public class Student {
    private Integer id; //оберточный тип, чтобы в отсутствии данных был null, (для правильной работы бд)
    private String name;
    private String lastName;
    private Integer age;
    private boolean isActive;

    public Student(Integer id, String name, String lastName, Integer age, boolean isActive) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isActive=" + isActive +
                '}';
    }
}
