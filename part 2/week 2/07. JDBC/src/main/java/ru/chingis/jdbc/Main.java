package ru.chingis.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    //language=SQL
    private static final String SQL_SELECT_STUDENTS = "select * from student";

    private static final String SQL_SELECT_STUDENT_BY_ID = "select * from student where id = ";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        Statement statement = connection.createStatement();

        ResultSet result = statement.executeQuery(SQL_SELECT_STUDENTS);

//        result.next();
//        System.out.println(result.getString(2));
//        result.next();
//        System.out.println(result.getString("name"));
//        result.next();
//        System.out.println(result.getString("name"));
//        result.next();
//        System.out.println(result.getString("name"));

        ResultSet anotherResult = statement.executeQuery(SQL_SELECT_STUDENT_BY_ID + " 3");

        anotherResult.next();
        System.out.println(anotherResult.getString("name"));


        // ADD to DB
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        String insertSql = "insert into student (name) values ('" + userName + "')";
        int affectedRows = statement.executeUpdate(insertSql);
        System.out.println(affectedRows);
    }
}
