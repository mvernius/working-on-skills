package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from student";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";


    private Connection connection;

    private RowMapper<Student> studentRowMapper = new RowMapper<Student>() {
        @Override
        public Student mapRow(ResultSet row) throws SQLException {
            return new Student(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("last_name"),
                    row.getInt("age"),
                    row.getBoolean("is_active")
            );
        }
    };

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Student findByName(String name) {
        return null;
    }

    @Override
    public void save(Student object) {

    }

    @Override
    public Student find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            return studentRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Student object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<Student> findAll() {
        try {
            List<Student> result = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Student student = studentRowMapper.mapRow(resultSet);
                result.add(student);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
