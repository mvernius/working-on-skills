package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonsRepositoryJdbcImpl implements LessonsRepository {

    private Connection connection;
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from lesson";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from lesson where id = ";


    private RowMapper<Lesson> lessonsRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getInt("course_id")
            );
        }
    };


    public LessonsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Lesson findByName(String name) {
        return null;
    }

    @Override
    public void save(Lesson object) {

    }

    @Override
    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            Lesson lesson = lessonsRowMapper.mapRow(resultSet);
            lesson.setCourse(new CoursesRepositoryJdbcImpl(connection).find(lesson.getCourseId()));
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(Lesson object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<Lesson> findAll() {
        try {
            List<Lesson> result = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Lesson lesson = lessonsRowMapper.mapRow(resultSet);
                lesson.setCourse(new CoursesRepositoryJdbcImpl(connection).find(lesson.getCourseId()));
                result.add(lesson);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
