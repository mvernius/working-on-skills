package ru.chingis.jdbc;

import ru.chingis.jdbc.models.Course;
import ru.chingis.jdbc.models.Lesson;
import ru.chingis.jdbc.models.Student;
import ru.chingis.jdbc.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class MainForRepositories {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(connection);
        Student thomas = studentsRepository.find(5);
        System.out.print("Should be Thomas: ");
        System.out.println(thomas);
        System.out.println("--------");
        List<Student> students = studentsRepository.findAll();
        //System.out.println(students);
        for (Student student : students) {
            System.out.println(student);
        }
        System.out.println();
        System.out.println("Courses Repository Test");
        CoursesRepository coursesRepository = new CoursesRepositoryJdbcImpl(connection);
        Course cs = coursesRepository.find(1);
        System.out.print("Should be CS: ");
        System.out.println(cs);
        System.out.println("-----------");
        List<Course> courses = coursesRepository.findAll();
        for (Course course : courses) {
            System.out.println(course);
        }

        System.out.println("Lessons Repository Test");
        LessonsRepository lessonsRepository = new LessonsRepositoryJdbcImpl(connection);
        Lesson algorithms = lessonsRepository.find(1);
        System.out.print("Should be algo: ");
        System.out.println(algorithms);
        System.out.println("-----------");
        List<Lesson> lessons = lessonsRepository.findAll();
        for (Lesson lesson : lessons) {
            System.out.println(lesson);
        }
    }
}
