package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
    Student findByName(String name);
}
