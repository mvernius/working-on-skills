package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Course;
import ru.chingis.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoursesRepositoryJdbcImpl implements CoursesRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from course";

//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "select * from course where id = ";
//
//    //language=SQL
//    private static final String SQL_SELECT_FROM_LESSON_BY_COURSE_ID = "select * from lesson where course_id = ";

    //language=SQL
    private static final String SQL_SELECT_FROM_COURSE_LESSON = "select * from course cs " +
            "join lesson l on cs.id = l.course_id";

    //language=SQL
    private static final String SQL_SELECT_FROM_COURSE_JOIN_LESSON_BY_ID = "select * from course cs " +
            "join lesson l on cs.id = l.course_id where course_id = ";

    private Connection connection;

    private RowMapper<Lesson> lessonsRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getInt("course_id")
            );
        }
    };

    private RowMapper<Course> coursesRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(
                    row.getInt("id"),
                    row.getString("title"),
                    row.getDate("start_date"),
                    row.getDate("end_date")
            );
        }
    };

    public CoursesRepositoryJdbcImpl(Connection connection) { this.connection = connection; }

    @Override
    public Course findByTitle(String title) {
        return null;
    }

    @Override
    public void save(Course object) {

    }

//    @Override
//    public Course find(Integer id) {
//        try {
//            List<Lesson> lessons = new ArrayList<>();
//            Statement statement = connection.createStatement();
//            ResultSet setForCourse = statement.executeQuery(SQL_SELECT_BY_ID + id);
//            setForCourse.next();
//            Course course = coursesRowMapper.mapRow(setForCourse);
//            ResultSet setForLesson = statement.executeQuery(SQL_SELECT_FROM_LESSON_BY_COURSE_ID + id);
//            while (setForLesson.next()) {
//                Lesson lesson = lessonsRowMapper.mapRow(setForLesson);
//                lesson.setCourse(course);
//                lessons.add(lesson);
//            }
//            course.setLessons(lessons);
//            return course;
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }


    @Override
    public Course find(Integer id) {
        try {
            List<Lesson> lessons = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_COURSE_JOIN_LESSON_BY_ID + id);
            resultSet.next();
            Course course = coursesRowMapper.mapRow(resultSet);
            lessons.add(lessonsRowMapper.mapRow(resultSet));
            while (resultSet.next()) {
                lessons.add(lessonsRowMapper.mapRow(resultSet));
            }
            course.setLessons(lessons);
            statement.close();
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Course object) {

    }

    @Override
    public void delete(Integer id) {

    }

//    @Override
//    public List<Course> findAll() {
//        try {
//            List<Course> result = new ArrayList<>();
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
//            Course course;
//            while (resultSet.next()) {
//                List<Lesson> lessons = new ArrayList<>();
//                course = coursesRowMapper.mapRow(resultSet);
//                ResultSet setForLesson = statement.executeQuery(SQL_SELECT_FROM_LESSON_BY_COURSE_ID + course.getId());
//                while (setForLesson.next()) {
//                    Lesson lesson = lessonsRowMapper.mapRow(setForLesson);
//                    lesson.setCourse(course);
//                    lessons.add(lesson);
//                }
//                course.setLessons(lessons);
//                result.add(course);
//            }
//            return result;
//        } catch (SQLException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }

    @Override
    public List<Course> findAll() {
        try (Statement statement = connection.createStatement()){
            Map<Integer, Course> coursesDict = new HashMap<>();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_COURSE_LESSON);
            while (resultSet.next()) {
                Integer course_id = resultSet.getInt("course_id");
                Course course = coursesDict.get(course_id);
                if (course == null) {
                    course = coursesRowMapper.mapRow(resultSet);
                    Lesson lesson = lessonsRowMapper.mapRow(resultSet);
                    course.setLessons(new ArrayList<>());
                    course.getLessons().add(lesson);
                    coursesDict.put(course_id, course);
                } else {
                    course.getLessons().add(lessonsRowMapper.mapRow(resultSet));
                }
            }
            return new ArrayList<>(coursesDict.values());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /*@Override
    public List<Course> findAll() {
        try (Statement statement = connection.createStatement();) {
            Map<Integer, Course> coursesDict = new HashMap<>();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_COURSE_LESSON);
            while (resultSet.next()) {
                Integer courseId = resultSet.getInt("course_id");
                coursesDict.computeIfAbsent(courseId, id -> {
                    Course newCourse = null;
                    try {
                        newCourse = coursesRowMapper.mapRow(resultSet);
                    } catch (SQLException throwables) {
                        throw new IllegalArgumentException(throwables);
                    }
                    newCourse.setLessons(new ArrayList<>());
                    return newCourse;
                }).getLessons().add(lessonsRowMapper.mapRow(resultSet));
            }
            return new ArrayList<>(coursesDict.values());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }*/
}
