package ru.chingis.jdbc.repositories;

import ru.chingis.jdbc.models.Lesson;

public interface LessonsRepository extends CrudRepository<Lesson> {
    Lesson findByName(String name);
}
