package com.informer;

import java.io.File;

public class Informer {
	public void print(String path) {
		int formatLength;
		try {
			File directory = new File(path);
			File[] files = directory.listFiles();
			formatLength = maxStringLengthOfFiles(files);
			for (File file : files) {
				beautifulPrint(file, formatLength);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}	
	}

	public String dirOrFile(File file) {
		String s;
		if (file.isDirectory()) {
			s = "dir";
		} else {
			s = "file";
		}
		return s;
	}

	public int maxStringLengthOfFiles(File[] files) {
		int max = files[0].getName().length();
		int length;
		for (File file : files) {
			length = file.getName().length();
			if (length > max) {
				max = length;
			}
		}
		return max;
	}

	public void beautifulPrint(File file, int formatLength) {
		String category = dirOrFile(file);
		double sizeKylobytes = (double)file.length() / 1000;
		System.out.printf("%" + formatLength + "s", file.getName());
		System.out.print(" | ");
		System.out.printf("%4s", category);
		System.out.println(" | " + sizeKylobytes + " kb");
	}
}