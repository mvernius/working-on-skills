package com.chingis.file_information.functions;

import com.chingis.file_information.functions.Informer;
import com.chingis.file_information.services.Arguments;
import com.chingis.file_information.services.FileInformation;
import com.chingis.file_information.services.ThreadService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ThreadRunner {
    public void mainPrint(Arguments argument) {
        Informer informer = new Informer();
        List<String> directories = argument.getDirectories();
        Map<String, List<FileInformation>> map = new ConcurrentHashMap<>();
        ThreadService threadService = new ThreadService();
        List<Thread> listOfThreads = new ArrayList<>();

        for (String name : directories) {
            Runnable someTask = () -> map.put(name, convertStringFileInfo(informer.generateInboundDirectoriesList(name)));
            listOfThreads.add(threadService.submit(someTask));
        }
        for (Thread thread : listOfThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                thread.currentThread().interrupt();
                throw new IllegalArgumentException(e);
            }
        }
        printMap(map);
    }

    public static List<FileInformation> convertStringFileInfo(List<String> array) {
        ArrayList<FileInformation> converted = new ArrayList<>();

        for (String name : array) {
            FileInformation fileInfo = new FileInformation(name);
            converted.add(fileInfo);
        }
        return converted;
    }

    public static void printMap(Map<String, List<FileInformation>> map) {
        for (Map.Entry<String, List<FileInformation>> entry : map.entrySet()) {
            System.out.println(entry.getKey());
            List<FileInformation> list = entry.getValue();
            for (FileInformation fileInfo : list) {
                System.out.println(fileInfo);
            }
            System.out.println();
        }
    }
}