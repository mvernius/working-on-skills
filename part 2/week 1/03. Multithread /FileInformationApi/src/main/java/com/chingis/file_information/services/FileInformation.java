package com.chingis.file_information.services;

public class FileInformation {
    private String information;

    public FileInformation(String information) {
        this.information = information;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @Override
    public String toString() {
        return this.information;
    }
}
