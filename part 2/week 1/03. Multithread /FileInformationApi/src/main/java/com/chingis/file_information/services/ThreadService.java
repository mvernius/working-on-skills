package com.chingis.file_information.services;

public class ThreadService {
    public Thread submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
        return thread;
    }
}
