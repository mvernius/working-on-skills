package com.chingis.file_information.functions;

import java.io.File;
import java.util.ArrayList;

public class Informer {
    public void print(String path) {
        try {
            File directory = new File(path);
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    String toPrint = generateStringInfo(file);
                    System.out.println(toPrint);
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public ArrayList<String> generateInboundDirectoriesList(String path) {
        try {
            File directory = new File(path);
            ArrayList<String> inboundDirArray = new ArrayList<>();
            File[] files = directory.listFiles();
            assert files != null;
            for (File file : files) {
                String intermediateResult = generateStringInfo(file);
                inboundDirArray.add(intermediateResult);
            }
            return inboundDirArray;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int maxNameLengthOfFiles(File[] files) {
        int max = files[0].getName().length();
        int length;
        for (File file : files) {
            length = file.getName().length();
            if (length > max) {
                max = length;
            }
        }
        return max;
    }

    public String generateStringInfo(File file) {
        String category;
        String fileName;
        String result = "";

        if (file.isDirectory()) {
            category = "dir";
        } else {
            category = "file";
        }
        double sizeKylobytes = (double)file.length() / 1000;
        fileName = file.getName();
        fileName = fillWhiteSpaces(fileName, 1);
        category = fillWhiteSpaces(category, 1);
        result += fileName + " | " + category;
        result += " | " + sizeKylobytes + " kb";

        return result;
    }


    //builder предложение идеи
    private String fillWhiteSpaces(String string, int quantity) {
        StringBuilder stringBuilder = new StringBuilder(string);
        for (int i = 0; i < quantity; i++) {
            stringBuilder.append(" ");
        }
        string = stringBuilder.toString();
        return string;
    }
}
