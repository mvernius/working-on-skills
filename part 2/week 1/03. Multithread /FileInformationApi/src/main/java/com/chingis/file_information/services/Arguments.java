package com.chingis.file_information.services;

import com.beust.jcommander.Parameter;
//import com.beust.jcommander.Parameters;
import java.util.ArrayList;
import java.util.List;

//@Parameters(separators = "=")
public class Arguments {

    @Parameter(names = "-path")
    private List<String> directories = new ArrayList<>();

    public List<String> getDirectories() {
        return this.directories;
    }
}
