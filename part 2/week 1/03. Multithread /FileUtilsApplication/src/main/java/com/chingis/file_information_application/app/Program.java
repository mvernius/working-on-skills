package com.chingis.file_information_application.app;

import com.beust.jcommander.JCommander;
import com.chingis.file_information.services.Arguments;
import com.chingis.file_information.functions.ThreadRunner;

public class Program {
    public static void main(String[] args) {
        Arguments argument = new Arguments();
        ThreadRunner threadRunner = new ThreadRunner();

        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        threadRunner.mainPrint(argument);
    }
}