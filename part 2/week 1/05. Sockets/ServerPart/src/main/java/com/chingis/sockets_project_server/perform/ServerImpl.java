package com.chingis.sockets_project_server.perform;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

public class ServerImpl implements Server{
    private ServerSocket serverSocket;
    private final List<ServerClientConnectionProcessor> listOfExecutables = new LinkedList<>();
    private final int port;

    public ServerImpl(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server successfully launched...");
            while (true) {
                Socket socketClient = serverSocket.accept();
                try {
                    listOfExecutables.add(new ServerClientConnectionProcessor(socketClient));
                } catch (IOException e) {
                    socketClient.close();
                }
            }
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void shutdown() throws IOException {
        for (Thread thread : listOfExecutables) {
            thread.interrupt();
        }
        if (serverSocket != null) {
            serverSocket.close();
        }
    }

    private class ServerClientConnectionProcessor extends Thread {
        private final BufferedReader income;
        private final PrintWriter outcome;

        public ServerClientConnectionProcessor(Socket socket) throws IOException {
            income = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outcome = new PrintWriter(socket.getOutputStream(), true);
            start();
        }

        @Override
        public void run() {
            String inputLine;
            try {
                while (true) {
                    inputLine = income.readLine();
                    if (inputLine.equals("stop")) {
                        break;
                    }
                    for (ServerClientConnectionProcessor toClient : listOfExecutables) {
                        toClient.send(inputLine);
                    }
                }
            } catch (IOException ignored) {}
        }

        private void send(String input) {
            outcome.println(Thread.currentThread().getName() + " tells: " + input);
        }
    }
}