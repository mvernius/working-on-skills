package com.chingis.sockets_project_server.perform;

public interface ThreadLauncher {
    public Thread submit(Runnable task);
}