package com.chingis.sockets_project_server.perform;

import com.beust.jcommander.JCommander;
import com.chingis.sockets_project_server.util.Arguments;

public class Program {
    public static void main(String[] args) {
        Arguments argument = new Arguments();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        int port = Integer.parseInt(argument.inputPort);
        Server server = new ServerImpl(port);
        server.run();
    }
}