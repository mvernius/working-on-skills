package com.chingis.sockets_project_server.perform;

import java.io.IOException;

public interface Server extends Runnable {
    void shutdown() throws IOException;
}