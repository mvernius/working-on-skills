package com.chingis.sockets_project_server.perform;

public class ThreadLauncherImpl implements ThreadLauncher{
    public Thread submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
        return thread;
    }
}