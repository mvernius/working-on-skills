package com.chingis.sockets_project_client.util;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {
    @Parameter(names = {"--serverPort"})
    public String inputPort;

    @Parameter(names = {"--serverHost"})
    public String host;
}
