package com.chingis.sockets_project_client.perform;

import java.io.*;
import java.net.Socket;

public class Client implements Runnable {
    private Socket socketClient;
    private BufferedReader reader;
    private BufferedWriter out;
    private ListenServer listenServer;


    public Client(String host, int port) {
        try {
            socketClient = new Socket(host, port);
            reader = new BufferedReader(new InputStreamReader(System.in));
            out = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
            listenServer = new ListenServer();
            listenServer.start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void run() {
        String clientMessage;
        try {
            try {
                while (true) {
                    clientMessage = reader.readLine();
                    if (clientMessage.equals("stop")) {
                        break;
                    }
                    out.write(clientMessage + "\n");
                    out.flush();
                    //System.out.println(in.readLine());
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            } finally {
                System.out.println("Client is closed...");
                socketClient.close();
                reader.close();
                out.close();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class ListenServer extends Thread {
        private BufferedReader in;

        ListenServer() {
            try {
                this.in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    String message = in.readLine();
                    if (message == null) {
                        break;
                    }
                    System.out.println(message);
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            } finally {
                try {
                    in.close();
                } catch (IOException ignored) { }
            }
        }
    }
}
