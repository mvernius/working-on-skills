package com.chingis.sockets_project_client.perform;

import com.beust.jcommander.JCommander;
import com.chingis.sockets_project_client.util.Arguments;

public class Program {
    public static void main(String[] args) {
        Arguments argument = new Arguments();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        int port = Integer.parseInt(argument.inputPort);
        Client client = new Client(argument.host, port);
        client.run();
    }
}
