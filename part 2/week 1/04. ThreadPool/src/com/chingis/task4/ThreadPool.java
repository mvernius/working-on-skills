package com.chingis.task4;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {
    private PoolWorker[] threads;
    private final Deque<Runnable> tasks;

    public ThreadPool(int countThreads) {
        if (countThreads > 0) {
            this.threads = new PoolWorker[countThreads];
            this.tasks = new LinkedList<>();
            for (int i = 0; i < countThreads; i++) {
                this.threads[i] = new PoolWorker();
                this.threads[i].start();
            }
        } else {
            tasks = null;
        }
    }

    public void shutdown() {
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    public class PoolWorker extends Thread {
        @Override
        public void run() {
            synchronized (tasks) {
                while (!Thread.currentThread().isInterrupted()) {
                    Runnable nextTask = tasks.poll();
                    if (nextTask != null) {
                        nextTask.run();
                    } else {
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                }
            }
        }
    }
}