package com.chingis.end.app;

import com.informer.Informer;
import com.beust.jcommander.JCommander;

class Program {
	public static void main(String[] args) {
		Arguments argument = new Arguments();


		JCommander.newBuilder()
			.addObject(argument)
			.build()
			.parse(args);

		Informer informer = new Informer();
		informer.print(argument.directory);
	}
}