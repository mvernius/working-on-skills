import java.util.Scanner;

class Program {
	//не классический атой, с учетом возможного некорректного ввода и отрицательных чисел. 
	public static int convertCharArrayToNumber(char[] array) {
		int res = 0;
		int i = 0;
		int sign = 0;
		while (array[i] < '0' && array[i] > '9' && array[i] != '-') {
			i++;
		}
		while (array[i] == '-') {
			i++;
			sign++;
		}
		sign = (sign % 2 == 0) ? 1 : -1;
		for (int j = i; j < array.length; ++j) {
			res = res * 10 + (array[j] - '0');
		}
		return res * sign;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char[] array = scanner.nextLine().toCharArray();
		int result = convertCharArrayToNumber(array);
		System.out.println(result);
	}
}