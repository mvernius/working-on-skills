import java.util.Scanner;
/*
	1. Считаю количество совпадений
	1.1 Использую сразу интовый массив
	1.2 Индекс массива - позиция буквы в алфавите
	2. Создаю просто массив алфавита
	3. Сортировка выбором на базе интового массива
	! не гибкое решение под другие языки. Только для англ.
	! логичнее использовать map.
*/
class Program {
	public static int[] countDuplications(char[] array) {
		int[] result = new int[52];
		int position;
		for (int i = 0; i < array.length; i++) {
			if (array[i] >= 'A' && array[i] <= 'Z') {
				position = array[i] - 'A';
				result[position] += 1;
			} else if (array[i] >= 'a' && array[i] <= 'z') {
				position = array[i] - 'a' + 26;
				result[position] += 1;
			}
		}
		return result;
	}

	public static char[] makeCharArray() {
		char[] newArray = new char[52];
		for (int i = 0; i < newArray.length; i++) {
			if (i < 26) {
				newArray[i] = (char)(i + 'A');
			} else {
				newArray[i] = (char)(i - 26 + 'a');
			}
		}
		return newArray;
	}

	public static void sortDescending(char[] arrayChar, int[] arrayNum) {
		for (int i = 0; i < arrayNum.length; i++) {
			for (int j = i + 1; j < arrayNum.length; j++) {
				if (arrayNum[j] > arrayNum[i]) {
					int tmpNum = arrayNum[i];
					arrayNum[i] = arrayNum[j];
					arrayNum[j] = tmpNum;
					char tmpChar = arrayChar[i];
					arrayChar[i] = arrayChar[j];
					arrayChar[j] = tmpChar;
				}
			}
		}
	}

	public static void printMostCommonLetters(char[] arrayLetters, int quantity) {
		for (int i = 0; i < quantity; i++) {
			System.out.println(arrayLetters[i]);
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char[] arrayString = scanner.nextLine().toCharArray();
		int[] duplications = countDuplications(arrayString);
		char[] alphabet = makeCharArray();
		sortDescending(alphabet, duplications);
		printMostCommonLetters(alphabet, 3);
	}
}