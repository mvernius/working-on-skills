import java.util.Scanner;

class Program {
	public static double pow(double x, int y) {
		double res = 1;
		for (int i = 0; i < y; i++) {
			res *= x;
		}
		return res;
	}

	public static double integralSimpsonMethod(double a, double b, double n, int y) {
		double h = (b - a) / n;
		double res = 0;
		double current = 0;
		for (double x = a + h; x <= b; x += 2 * h) {
			current = pow((x - h), y) + (4 * pow(x, y)) + pow((x + h), y);
			res += current;
		}
		return res * (h / 3);
	}

	public static double integralRectangleMethod(double a, double b, double n, int y) {
		double h = (b - a) / n;
		double res = 0;
		double currentRectangle = 0;
		for (double x = a; x <= b; x += h) {
			currentRectangle = pow(x, y) * h;
			res += currentRectangle;
		}
		return res;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		String nameSimpson = "Simpson";
		String nameRectangle = "Rectangle";
		int ns[] = {100, 1000, 10000, 100000, 1000000}; // разбиения
		int ys[] = {2, 3, 4, 5, 6, 7}; // степени

		for (int i = 0; i < ns.length; i++) {
			double n = ns[i];
			for (int j = 0; j < ys.length; j++) {
				int y = ys[j];
				double simpson = integralSimpsonMethod(a, b, n, y);
				double rectangle = integralRectangleMethod(a, b, n, y);
				// Simpson - 100 - x^2 = значение
				System.out.printf("%-10s - %-8d - x^%d = %f%n", nameSimpson, ns[i], y, simpson);
				System.out.printf("%-10s - %-8d - x^%d = %f%n", nameRectangle, ns[i], y, rectangle);
			}
		}
	}
}