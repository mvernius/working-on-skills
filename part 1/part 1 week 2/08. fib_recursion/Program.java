import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int serialNumber = scanner.nextInt();
		if (serialNumber > 2) {
			System.out.println(fibonacciRecursion(serialNumber, 3, 1, 1));
		} else if (serialNumber > 0) {
			System.out.println(1);
		} else if (serialNumber == 0) {
			System.out.println(0);
		} else {
			System.out.println("Incorrect number.");
		}

	}

	public static int fibonacciRecursion(int serialNumber, int currentPosition, int beforeLast, int lastNumber) {
		if (currentPosition > serialNumber) {
			return lastNumber;
		} else {
			int newLast = lastNumber + beforeLast;
			currentPosition++;
			return fibonacciRecursion(serialNumber, currentPosition, lastNumber, newLast);
		}
	}
}