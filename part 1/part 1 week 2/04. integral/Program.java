import java.util.Scanner;

class Program {
	public static double ft(double x) {
		return x * x;
	}

	public static double integralSimpsonMethod(double a, double b, int n) {
		double h = (b - a) / n;
		double res = 0;
		double current = 0;
		for (double x = a + h; x <= b; x += 2 * h) {
			current = ft(x - h) + (4 * ft(x)) + ft(x + h);
			res += current;
		}
		return res * (h / 3);
	}

	public static double integralRectangleMethod(double a, double b, int n) {
		double h = (b - a) / n;
		double res = 0;
		double currentRectangle = 0;
		for (double x = a; x <= b; x += h) {
			currentRectangle = ft(x) * h;
			res += currentRectangle;
		}
		return res;
	}

	/*
		мне не нравится оформления мейника. Хочу придумать способ вывода всей таблицы в цикле. 
		Кроме как использовать двойной массив (по факту тройной) ничего не придумал. Каждая строка таблицы - отдельный массив.
		Также, как добиться форматированния строки с помощью переменной? Генерация строки с %, флагами и значением?
	*/
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		int n = scanner.nextInt();

		double resultSimpsonMethod = integralSimpsonMethod(a, b, n);
		double resultRectangleMethod = integralRectangleMethod(a, b, n);
		String operationsQuantity = makeStringCentered(insertSpaceBetweenRanks("" + n), 19);

		printTable("НАЗВАНИЕ МЕТОДА", "КОЛИЧЕСТВО ИТЕРАЦИЙ", "РЕЗУЛЬТАТ ВЫЧИСЛЕНИЙ");
		printTable("Метод Симпсона", operationsQuantity, makeStringCentered("" + resultSimpsonMethod, 20));
		printTable("Метод прямоугольников", operationsQuantity, makeStringCentered("" + resultRectangleMethod, 20));
	}

	/* 
	далее реализованы не требовавшиеся по заданию функции.
	Сделаны только ради любопытства и удобства автора. 
	*/
	public static String fillWhiteSpaces(String s, int quantity) {
		for (int i = 0; i < quantity; ++i) {
			s += " ";
		}
		return s;
	}

	public static String makeStringCentered(String s, int width) {
		String resString = "";
		int quantitySpaces = 0;
		if (s.length() < width) {
			quantitySpaces = (width - s.length()) / 2;
		}
		resString = fillWhiteSpaces(resString, quantitySpaces);  //вставка в конец массива затратная операция. 
		resString += s;
		resString = fillWhiteSpaces(resString, quantitySpaces);
		return resString;
	}

	public static String insertSpaceBetweenRanks(String s) {
		String res = "";
		for (int i = 0; i < s.length(); i++) {
			res += s.charAt(i);
			if (s.length() > 3) {
				if (i % 3 == 0) {
					res += " ";
				}	
			}	
		}
		return res;
	}

	public static void printTable(String firstColumn, String secondColumn, String thirdColumn) {
		System.out.printf("%-22s | %-19s | %-20s%n", firstColumn, secondColumn, thirdColumn);
	}
}