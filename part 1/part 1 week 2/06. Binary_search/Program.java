import java.util.Scanner;

class Program {
	public static void selectionSort(int[] array, boolean fromMinToMax) {
		if (fromMinToMax) {
			for (int i = 0; i < array.length - 1; i++) {
				for (int j = i + 1; j < array.length; j++) {
					if (array[i] > array[j]) {
						int tmp = array[i];
						array[i] = array[j];
						array[j] = tmp;
					}
				}
			}
		} else {
			for (int i = 0; i < array.length - 1; ++i) {
				for (int j = i + 1; j < array.length; ++j) {
					if (array[i] < array[j]) {
						int tmp = array[i];
						array[i] = array[j];
						array[j] = tmp;
					}
				}
			}
		}
	}

	public static void binarySearch(int[] array, int numberToSearch) {
		int left = 0;
		int right = array.length - 1;
		int index = 0;
		int mid;
		boolean flag = false;
		String result;
		while (left <= right) {
			mid = (left + right) / 2;
			if (array[mid] < numberToSearch) {
				left = mid + 1;
			} else if (array[mid] > numberToSearch) {
				right = mid - 1;
			} else {
				index = mid;
				flag = true;
				break;
			}
		}
		if (flag) {
			System.out.println("Number " + numberToSearch + " exists in array with index: " + index);
		} else {
			System.out.println("Number doesn't exist");
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number of elements in array");
		int quantityElements = scanner.nextInt();
		int[] array = new int[quantityElements];
		System.out.println("Fill the array " + quantityElements + " times");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println("Type the number, you'd like to search");
		int numberToSearch = scanner.nextInt();
		selectionSort(array, true);
		binarySearch(array, numberToSearch);
	}
}