import java.util.Scanner;

class Program {
	public static void selectionSort(int[] array, boolean fromMinToMax) {
		if (fromMinToMax) {
			for (int i = 0; i < array.length - 1; i++) {
				for (int j = i + 1; j < array.length; j++) {
					if (array[i] > array[j]) {
						int tmp = array[i];
						array[i] = array[j];
						array[j] = tmp;
					}
				}
			}
		} else {
			for (int i = 0; i < array.length - 1; ++i) {
				for (int j = i + 1; j < array.length; ++j) {
					if (array[i] < array[j]) {
						int tmp = array[i];
						array[i] = array[j];
						array[j] = tmp;
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int quantityElements = scanner.nextInt();
		int[] array = new int[quantityElements];
		for (int i = 0; i < quantityElements; ++i) {
			array[i] = scanner.nextInt();
		}
		selectionSort(array, false);
		for (int i : array) {
			System.out.print(i + ", ");
		}
	}
}