import java.util.Scanner;

//по ощущениям намудрил и можно было проще сделать.
class Program {
	public static int compareStrings(char[] a, char[] b) {
		int i = 0;
		int difference = 0;
		while (i < a.length && i < b.length) {
			difference = a[i] - b[i];
			if (difference != 0) {
				break;
			}
			i++;
		}
		if (difference != 0) {
			difference = (difference < 0) ? -1 : 1;
		} else if (difference == 0 && a.length != b.length) { 
			difference = (a.length < b.length) ? -1 : 1;
		}
		return difference;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char[] a = scanner.nextLine().toCharArray();
		char[] b = scanner.nextLine().toCharArray();
		System.out.println(compareStrings(a, b));
	}
}