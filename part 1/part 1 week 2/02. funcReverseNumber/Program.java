import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		System.out.println(reverseNumber(number));
	}

	public static int reverseNumber(int number) {
		int res = 0;
		while (number > 0) {
			res = res * 10 + number % 10;
			number /= 10;
		}
		return res;
	}
}