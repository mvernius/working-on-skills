import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int[] array = new int[17];
		int index = 0, i = 0;
		while (number != 0) {
			array[i] = number % 2;
			number /= 2;
			if (number == 0) {
				index = i;
			}
			i++;
		}
		int res = 0;
		while (index >= 0) {
			res = 10 * res + array[index]; // if we need to store value for future usage.
			System.out.print(array[index]);
			index--;
		}
	}
}