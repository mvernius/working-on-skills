import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		System.out.println(countSumInRange(a, b));
	}

	// Counts sum of elements in section excluding borders
	public static int countSumInRange(int start, int end) {
		int sum = 0;
		while (start + 1 < end) {
			sum += start + 1;
			start++;
		}
		return sum;
	}

	// Counts sum of elements in section with borders
	public static int countSumInRangeWithBorders(int start, int end) {
		int sum = 0;
		while (start <= end) {
			sum += start;
			start++;
		}
		return sum;
	}
}