import java.util.Scanner;

class program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int res = 0;
		while (a / 10 > 0 || a % 10 > 0) {
			res = res * 10 + a % 10;
			a /= 10;
		}
		System.out.println(res);
	}
}