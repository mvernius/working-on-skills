import java.util.Scanner;

class program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int composition = 1;
		int number = scanner.nextInt();
		while (number != -1)
		{
			if (isSumDigitsEven(number)) 
			{
				composition *= number;
			}
			number = scanner.nextInt();
		}
		if (composition == 1)
		{
			composition = 0;
		}
		System.out.println("Composition of numbers with even sum of digits is: "
								+ composition);
	}

	public static boolean isSumDigitsEven(int number) {
		number = makePositive(number);
		int sum = countSumDigits(number);
		if (sum % 2 == 0) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	public static int countSumDigits(int number) {
		int sum = 0;
		while (number > 0) 
		{
			sum += number % 10;
			number /= 10;
		}
		return sum;
	}

	public static int makePositive(int number) {
		if (number < 0) {
			number *= -1;
		}
		return number;
	}
}