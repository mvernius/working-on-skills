import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int[] array = new int[17];
		int index = 0, i = 0;
		while (number != 0) {
			array[i] = number % 2;
			number /= 2;
			if (number == 0) {
				index = i;
			}
			i++;
		}
		while (index >= 0) {
			System.out.print(array[index]);
			index--;
		}
	}
}