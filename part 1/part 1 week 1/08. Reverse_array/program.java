import java.util.Scanner;

class program {
	public static void main(String[] args) {
		int[] array = readArray();
		reverseArray(array);
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

	public static int[] readArray() {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int[] array = new int[length];
		for (int i = 0; i < length; ++i)
		{
			array[i] = scanner.nextInt();
		}
		return array;
	}

	public static void reverseArray(int[] array) {
		int tmp;
		for (int i = 0, j = array.length - 1; i < array.length / 2; ++i, --j) {
			tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}
}