import java.util.Scanner;

class program {
	public static void main(String[] args) {
		double result;
		int[] array = readArray();
		result = countArrayArithmeticMean(array);
		outputMessage(result, array);
	}

	public static int[] readArray() {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int[] array = new int[length];
		for (int i = 0; i < length; ++i)
		{
			array[i] = scanner.nextInt();
		}
		return array;
	}

	public static double countArrayArithmeticMean(int[] array) {
		double sum = 0;
		int i = 0;
		while (i < array.length) 
		{
			sum += array[i];
			i++;
		}
		return sum / i;
	}

	public static void outputMessage(double result, int[] array) {
		String message;
		if (array.length == 1)
		{
			message = "result for this one number is ";
		}
		else
		{
			message = "result for this " + array.length + " numbers is ";
		}
		System.out.format(message + "%.2f%n", result);
	}
}