import java.util.Scanner;

class program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int length = scanner.nextInt();
		int[] array = new int[length];
		for (int i = 0; i < length; ++i) {
			array[i] = scanner.nextInt();
		}
		changeMinMaxArray(array);
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

	public static void changeMinMaxArray(int[] array) {
		int min = array[0];
		int max = array[0];
		for (int i = 0; i < array.length; ++i) {
			if (array[i] < min)
			{
				min = array[i];
			}
			if (array[i] > max)
			{
				max = array[i];
			}
		}
		for (int i = 0; i < array.length; ++i) {
			if (array[i] == min)
			{
				array[i] = max;
			}
			else if (array[i] == max)
			{
				array[i] = min;
			}
		}
	}
}