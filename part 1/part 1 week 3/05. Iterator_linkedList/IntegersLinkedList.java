class IntegersLinkedList {
	private Node first;
	private Node last;
	private int count;

	public IntegersLinkedList() {
		this.count = 0;
	}


	private static class Node {
		private int value;
		private Node next;

		public Node(int value) {
			this.value = value;
		}
	}


	public class Iterator {
		Node current;
		int iterator;

		Iterator() {
			this.iterator = 0;
		}

		boolean hasNext() {
			if (iterator < count) {
				if (iterator == 0) {
					this.current = first;
				}
				return true;
			} else {
				iterator = 0;
				return false;
			}
		}

		int next() {
			int nextElem = current.value;
			if (count - 1 != iterator) {
				current = current.next;
			} 
			iterator++;
			return nextElem;
		}
	}


	public void add(int value) {
		Node newNode = new Node(value);
		if (first == null) {
			first = newNode;
			last = newNode;
		} else {
			this.last.next = newNode;
			this.last = newNode;
		}
		count++;
	}


	public void add(int value, int index) {
		Node newNode = new Node(value);
		if (index >= 0 && index <= count) {
			Node current = first;
			if (index == 0) {
				newNode.next = first;
				first = newNode;
			} else if (index < count) {
				for (int i = 1; i < index; i++) {
					current = current.next;
				}
				Node saveConnectionWithNext = current.next;
				newNode.next = saveConnectionWithNext;
				current.next = newNode;
			} else {
				while (current.next != null) {
					current = current.next;
				}
				current.next = newNode;
			}
			count++;
		} else {
			System.err.println("index out of range");
		}
	}


	public int get(int index) {
		if (index >= 0 && index < count) {
			Node current = first;
			for (int i = 0; i < index; i++) {
				current = current.next;
			}
			return current.value;
		} else {
			System.err.println("index out of range");
			return -1;
		}
	}


	public void remove(int index) {
		Node current = first;
		if (index >= 0 && index < count) {
			if (index == 0) {
				first = first.next;
				current = null;
			} else {
				for (int i = 0; i < index - 1; i++) {
					current = current.next;
				}
				Node tmpToRemove = current.next;
				if (index < count - 1) {
					Node tmpToSurvive = tmpToRemove.next;
					current.next = tmpToSurvive;
					tmpToRemove.next = null; //саму переменную не обнуляю, потому что она без связей. вроде Garbage collector заберет.
				} else {
					current.next = null;
				}
			}
			count--;
		} else {
			System.err.println("Index out of range");
		}
	}


	public void reverse() {
		Node previousNode = null;
		Node nextNode = null;
		Node current = first;

		while (current != null) {
			nextNode = current.next;
			current.next = previousNode;
			previousNode = current;
			current = nextNode;
		}
		last = first;
		first = previousNode;
	}
}