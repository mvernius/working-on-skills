class Test {
	public static void main(String[] args) {
		IntegersLinkedList list = new IntegersLinkedList();

		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
		//list.add(0);

		IntegersLinkedList.Iterator iterator = list.new Iterator();
		System.out.println("-- Show List --");
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println();
		System.out.println("-- Show reverse --");
		list.reverse();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println("-- additional 10 in the end --");	
		list.add(10);
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println("-- Value 9 must be removed -- ");
		list.remove(0);
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println("-- adding Value 100 in the middle --");
		list.add(100, 5);
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println();
		System.out.println("List with one element");
		System.out.println();
		IntegersLinkedList list1 = new IntegersLinkedList();
		IntegersLinkedList.Iterator iterator1 = list1.new Iterator();
		list1.add(0);
		while(iterator1.hasNext()) {
			System.out.println(iterator1.next());
		}
		list1.reverse();
		while(iterator1.hasNext()) {
			System.out.println(iterator1.next());
		}
	}
}