//package src;

import java.util.Random;

class Program {
	public static void main(String[] args) {
		Random random = new Random();
		Human[] humans = new Human[100];
		int[] countDuplicatedAge = new int[121];
		for (int i = 0; i < humans.length; i++) {
			humans[i] = new Human();
			humans[i].age = random.nextInt(121);
			countDuplicatedAge[humans[i].age] += 1;
		}
		int max = countDuplicatedAge[0];
		int index = 0;
		for (int i = 0; i < countDuplicatedAge.length; i++) {
			if (max < countDuplicatedAge[i]) {
				max = countDuplicatedAge[i];
				index = i;
			}
		}
		System.out.println("the most popular age is - " + index + " | It's quantity - " + countDuplicatedAge[index]);
	}
}