class IntegersLinkedList {
	private Node first;
	private int count;

	public IntegersLinkedList() {
		this.count = 0;
	}

	public void add(int value) {
		Node newNode = new Node(value);
		if (first == null) {
			first = newNode;
		} else {
			Node current = first;
			while (current.getNext() != null) {
				current = current.getNext();
			}
			current.setNext(newNode);
		}
		count++;
	}

	public void add(int value, int index) {
		Node newNode = new Node(value);
		if (index >= 0 && index <= count) {
			Node current = first;
			if (index == 0) {
				newNode.setNext(first);
				first = newNode;
			} else if (index < count) {
				for (int i = 1; i < index; i++) {
					current = current.getNext();
				}
				Node saveConnectionWithNext = current.getNext();
				newNode.setNext(saveConnectionWithNext);
				current.setNext(newNode);
			} else {
				while (current.getNext() != null) {
					current = current.getNext();
				}
				current.setNext(newNode);
			}
			count++;
		} else {
			System.err.println("index out of range");
		}
	}

	public int get(int index) {
		if (index >= 0 && index < count) {
			Node current = first;
			for (int i = 0; i < index; i++) {
				current = current.getNext();
			}
			return current.getValue();
		} else {
			System.err.println("index out of range");
			return -1;
		}
	}

	public void remove(int index) {
		Node current = first;
		if (index >= 0 && index < count) {
			if (index == 0) {
				first = first.getNext();
				current = null;
			} else {
				for (int i = 0; i < index - 1; i++) {
					current = current.getNext();
				}
				Node tmpToRemove = current.getNext();
				if (index < count - 1) {
					Node tmpToSurvive = tmpToRemove.getNext();
					current.setNext(tmpToSurvive);
					tmpToRemove.setNext(null); //саму переменную не обнуляю, потому что она без связей. вроде Garbage collector заберет.
				} else {
					current.setNext(null);
				}
			}
			count--;
		} else {
			System.err.println("Index out of range");
		}
	}

	public void reverse() {
		Node previousNode = null;
		Node nextNode = null;
		Node current = first;

		while (current != null) {
			nextNode = current.getNext();
			current.setNext(previousNode);
			previousNode = current;
			current = nextNode;
		}
		first = previousNode;
	}
}