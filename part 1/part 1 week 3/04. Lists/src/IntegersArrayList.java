class IntegersArrayList {
	private static final int initialArraySize = 10;
	private int count;
	private int[] elements;

	public IntegersArrayList() {
		count = 0;
		elements = new int[initialArraySize];
	}

	public class Iterator {
		int current;

		public Iterator() {
			this.current = 0;
		}

		boolean hasNext() {
			return current < count;
		}

		int next() {
			int nextVal = elements[current];
			current++;
			return nextVal;
		}
	}

	private int[] resize(int[] elements, double size) {
		int actualSize = (int)size;
		int[] copy = new int[actualSize];

		if (copy.length > elements.length) {
			for (int i = 0; i < elements.length; i++) {
				copy[i] = elements[i];
			}
		} else {
			for (int i = 0; i < copy.length; i++) {
				copy[i] = elements[i];
			}
		}
		return copy;
	}

	public void add(int value) {
		if (count < elements.length) {
			elements[count] = value;
		} else {
			elements = resize(elements, elements.length * 1.5);
			elements[count] = value;
		}
		count++;
	}

	

	public int length() {
		return elements.length;
	}

	public int get(int index) {
		if (index >= 0 && index < elements.length) {
			return elements[index];
		} else {
			System.err.println("index out of range");
			return -1;
		}
	}

	public void add(int value, int index) {
		if (index >= 0 && index < elements.length) {
			if (count + 1 >= elements.length) {
				elements = resize(elements, elements.length * 1.5);
			}
			for (int secondCount = elements.length - 1; secondCount > index; secondCount--) {
				elements[secondCount] = elements[secondCount - 1];
			}
			elements[index] = value;
			count++;
		} else {
			System.err.println("index out of range");
		}
	}

	public void remove(int index) {
		if (index >= 0 && index < count) {
			if (index < elements.length - 1) {
				for (int i = index; i < elements.length - 1; i++) {
					elements[i] = elements[i + 1];
				}
			}
			count--;
			elements = resize(elements, elements.length - 1);
		} else {
			System.err.println("Index out of range");
		}
	}

	public void reverse() {
		int tmp;
		System.out.println(elements.length + " this is length");
		for (int i = 0, j = elements.length - 1; i < elements.length / 2; ++i, --j) {
			tmp = elements[i];
			elements[i] = elements[j];
			elements[j] = tmp;
		}
	}
}