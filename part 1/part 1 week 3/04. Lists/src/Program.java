class Program {
	public static void main(String[] args) {
		IntegersArrayList arrayList = new IntegersArrayList();
		IntegersLinkedList linkedList = new IntegersLinkedList();

		for (int i = 0; i < 15; i++) {
			arrayList.add(i);
			linkedList.add(i);
		}
		System.out.printf("check if lists are created. Elem index = elem value %n");
		for (int i = 0; i < arrayList.length(); i++) {
			System.out.println("Array " + i + " element is " + arrayList.get(i));
			System.out.println("List " + i + " element is " + linkedList.get(i));
		}

		arrayList.add(100, 5);
		linkedList.add(100, 5);
		System.out.printf("%n%nSee if 100 on 5th place %n");
		for (int i = 0; i < arrayList.length(); i++) {
			System.out.println("Array " + i + " element is " + arrayList.get(i));
			System.out.println("List " + i + " element is " + linkedList.get(i));
		}

		arrayList.remove(0);
		linkedList.remove(0);
		System.out.printf("%n%n0 element with value 0 should be deleted %n");
		for (int i = 0; i < arrayList.length(); i++) {
			System.out.println("Array " + i + " element is " + arrayList.get(i));
			System.out.println("List " + i + " element is " + linkedList.get(i));
		}

		System.out.printf("%n%nNow lists should be reversed %n");
		arrayList.reverse();
		linkedList.reverse();
		for (int i = 0; i < arrayList.length(); i++) {
			System.out.println("Array " + i + " element is " + arrayList.get(i));
			System.out.println("List " + i + " element is " + linkedList.get(i));
		}


		System.out.printf("%n%nChecking for errors %n");
		arrayList.add(1000, 100);
		linkedList.add(1000, 100);
		int i = arrayList.get(20);
		i = linkedList.get(20);
		arrayList.remove(20);
		linkedList.remove(16);
	}
}