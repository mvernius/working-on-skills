class Human {
	private String firstName;
	private String lastName;
	private boolean sex;
	private int age;
	private double salary;
	private String address;
	
	private Human() {

	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean getSex() {
		return sex;
	}

	public int getAge() {
		return age;
	}

	public double getSalary() {
		return salary;
	}

	public String getAddress() {
		return address;
	}
	/*
	public String setFirstName(String name) {
		firstName = name;
	}

	public String setLastName(String surname) {
		lastName = surname;
	}

	public boolean getSex(boolean sex) {
		this.sex = sex;
	}

	public int setAge(int age) {
		if (age > 0) {
			this.age = age;
		}
	}

	public double setSalary(double salary) {
		if (salary > 0) {
			this.salary = salary;
		}
	}

	public String setAddress(String address) {
		this.address = address;
	}
	*/

	public static Builder builder() {
		return new Human().new Builder();
	}

	//получается, использование такого билдера все время создает нового человека?
	//когда обращаемся к билдеру, создается подвешенный объект, к которому доступа нет. поэтому завершаем билдом?

	public class Builder {
		private Builder() {

		}

		public Builder firstName(String firstName) {
			Human.this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			Human.this.lastName = lastName;
			return this;
		}

		public Builder sex(boolean sex) {
			Human.this.sex = sex;
			return this;
		}

		public Builder age(int age) {
			Human.this.age = age;
			return this;
		}

		public Builder salary(double salary) {
			Human.this.salary = salary;
			return this;
		}

		public Builder address(String address) {
			Human.this.address = address;
			return this;
		}

		public Human build() {
			return Human.this;
		}
	}
}