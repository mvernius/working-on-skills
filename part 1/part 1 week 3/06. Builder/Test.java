class Test {
	public static void main(String[] args) {
		Human human = Human.builder()
		.age(24)
		.firstName("Chingis")
		.lastName("Bakhtevaleev")
		.sex(true)
		.build();
							
		System.out.println(human.getFirstName());
		System.out.println(human.getLastName());
		System.out.println(human.getAddress());
		System.out.println(human.getSalary());
		System.out.println(human.getSex());
		System.out.println(human.getAge());
	}
}