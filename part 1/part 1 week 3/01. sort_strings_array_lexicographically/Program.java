import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		char[][] array = makeArray();
		sortLexicographically(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

	/*
		1. Столкнулся с багом (особенностью) Scanner;
		2. не до конца понимаю, как работает
		3. Если не создавать прослойку для result[0] (22 строка), 
			то в цикле на 0 элементе, как будто сначала закрывается,
			 и на 1 индексе i Scanner снова открывается 
			и начинает работать корректно. 
	*/
	public static char[][] makeArray() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Type rows quantity: ");
		int rows = scanner.nextInt();
		scanner.nextLine();
		char[][] result = new char[rows][];
		for (int i = 0; i < result.length; i++) {
			System.out.println("Type your string for row " + i);
			result[i] = scanner.nextLine().toCharArray();
		}
		return result;
	}

	public static int strCompare(char[] a, char[] b) {
		int i = 0;
		int difference = 0;
		while (i < a.length && i < b.length) {
			difference = a[i] - b[i];
			if (difference != 0) {
				break;
			}
			i++;
		}
		if (difference != 0) {
			difference = (difference < 0) ? -1 : 1;
		} else if (difference == 0 && a.length != b.length) { 
			difference = (a.length < b.length) ? -1 : 1;
		}
		return difference;
	}

	/*
	public static char[] strCopy(char[] source) {
		char[] destination = new char[source.length];
		for (int i = 0; i < source.length; i++) {
			destination[i] = source[i];
		}
		return destination;
	}

		1. Сначала реализовал через копирование
		2. потом внимательно послушал про особенность джавы
		3. решил протестировать вариант с простой заменой по адресу
		4. Оставил, как итоговое решение. 
		5. По сути меняем адреса указателей, но неочевидно, потому что посмотреть адресацию не могу в джаве. 
	*/
	public static void sortLexicographically(char[][] array) {
		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				int difference = strCompare(array[i], array[j]);
				if (difference == 1) {
					/*
					char[] tmp = strCopy(array[i]);
					array[i] = strCopy(array[j]);
					array[j] = strCopy(tmp);
					*/
					char[][] tmp = new char[1][];
					tmp[0] = array[i];
					array[i] = array[j];
					array[j] = tmp[0];
				}
			}
		}
	}
}