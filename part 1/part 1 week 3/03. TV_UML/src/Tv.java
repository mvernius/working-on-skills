import java.util.Random;

class Tv {
	Random random = new Random();

	private String nameModel;
	private Channel[] channels;
	private final int MAX_CHANNELS = 500 - random.nextInt(500);
	private int channelCount;
	private RemoteController remoteController;
	private int count = 0;

	Tv(String nameModel) {
		this.nameModel = nameModel;
		this.channels = new Channel[MAX_CHANNELS];
		channelCount = 0;
	}

	void addChannel(Channel channel) {
		if (count < MAX_CHANNELS) {
			this.channels[count] = channel;
			count++;
		}
	}

	public Channel getChannel(int channelNumber) {
		if (channelNumber < MAX_CHANNELS) {
			return this.channels[channelNumber];
		} else {
			System.out.println("Channel doesn't exist");
			return null;
		}
	}

	void setRemoteController(RemoteController remoteController) {
		this.remoteController = remoteController;
	}

	public int getMaxChannels() {
		return MAX_CHANNELS;
	}
}