import java.util.Random;

class Channel {
	Random random = new Random();
	private Program[] programs;
	private final int MAX_PROGRAM = 100 - random.nextInt(100);
	private int count;
	private String name;
	private int channelNumber;
	private Tv tv;

	Channel(String name) {
		this.name = name;
		this.programs = new Program[MAX_PROGRAM];
		this.count = 0;
	}

	void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}

	void addProgram(Program program) {
		if (count < MAX_PROGRAM) {
			this.programs[count] = program;
			program.setChannel(this);
			this.count++;
		}
	}

	public Program getCurrentProgram() {
		int programsLength = this.programs.length - 1;
		int randomProgram = programsLength - random.nextInt(programsLength);
		return this.programs[randomProgram];
	}

	public int getMaxPrograms() {
		return MAX_PROGRAM;
	}
}