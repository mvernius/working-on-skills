import java.util.Random;
import java.util.Scanner;

class Test {
	public static final int MIN_SMALL_ASCII = 'a';
	public static final int MIN_CAPITAL_ASCII = 'A';
	public static void main(String[] args) {
		Tv tv = new Tv("LG");
		int maxChannels = tv.getMaxChannels();
		int maxPrograms;
		Channel channel;
		Program program;
		Scanner scanner = new Scanner(System.in);

		for (int i = 0; i < maxChannels; i++) {
			channel = new Channel(createString(true));
			tv.addChannel(channel);
			maxPrograms = channel.getMaxPrograms();
			for (int j = 0; j < maxPrograms; j++) {
				program = new Program(createString(false));
				channel.addProgram(program);
			}
		}

		RemoteController remoteController = new RemoteController("LG");
		remoteController.setTv(tv);
		tv.setRemoteController(remoteController);
		remoteController.on(scanner.nextInt());
	}

	// метод псевдослучайно генерации строк, для того, чтобы ручками не работать.
	//bolean state true - для каналов, false для програм
	public static String createString(boolean state) {
		String result = "";
		Random random = new Random();
		char c;
		int randomStringLength;

		if (state) {
			randomStringLength = random.nextInt(10);
		} else {
			randomStringLength = random.nextInt(10) + 10;
		}
		for (int i = 0; i < randomStringLength; i++) {
			if (i % (random.nextInt(3) + 1) == 0) {
				c = (char)(random.nextInt(25) + MIN_CAPITAL_ASCII);
			} else {
				c = (char)(random.nextInt(25) + MIN_SMALL_ASCII);
			}
			result += c;
		}
		return result;
	}
}