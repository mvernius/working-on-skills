class RemoteController {
	private String name;
	private Tv tv;

	RemoteController(String name) {
		this.name = name;
	}

	void setTv(Tv tv) {
		this.tv = tv;
	}


	// try - catch нужно реализовать здесь. Так, как я описал, null не обрабатывается.
	public void on(int channelNumber) {
		Channel channel = tv.getChannel(channelNumber);
		Program program = channel.getCurrentProgram();
		if (program != null) {
			System.out.println(program.toString());
		} else {
			System.out.println("It seems like this channel doesn't exist");
		}
	}
}