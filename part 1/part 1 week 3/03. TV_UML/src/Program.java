class Program {
	private Channel channel;
	private String name;

	Program(String name) {
		this.name = name;
	}

	void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override 
	public String toString() {
		return this.name;
	}
}