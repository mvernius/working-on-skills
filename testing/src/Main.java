public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();
        Animal lion = new Lion();
        Animal pig = new Pig();
        animal.execute();
        lion.execute();
        pig.execute();
    }
}
