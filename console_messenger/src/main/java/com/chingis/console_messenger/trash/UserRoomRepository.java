package com.chingis.console_messenger.trash;

import com.chingis.console_messenger.server.repositories.CrudRepository;

public interface UserRoomRepository extends CrudRepository<UserRoom> {
}
