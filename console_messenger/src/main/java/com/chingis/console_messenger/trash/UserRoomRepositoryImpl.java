package com.chingis.console_messenger.trash;

import com.chingis.console_messenger.server.repositories.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

//only for restoring user session
public class UserRoomRepositoryImpl implements UserRoomRepository {
    private final Connection CONNECTION;

    //language=SQL
    private final static String SQL_INSERT = "insert into usr_room (id_user, id_room) values (?, ?)";
    //language=SQL
    private final static String SQL_FIND_BY_ID = "select * from usr_room where id = (?)";
    //language=SQL
    private final static String SQL_FIND_BY_USER_ID = "select * from usr_room where id_user = (?)";
    //language=SQL
    private final static String SQL_DELETE_BY_USER_ID = "delete from usr_room where id_user = (?)";
    //language=SQL
    private final static String SQL_FIND_ALL = "select * from usr_room";

    public UserRoomRepositoryImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private final RowMapper<UserRoom> userRoomRowMapper = row -> new UserRoom(
            row.getLong("id_user"),
            row.getLong("id_room")
    );

    @Override
    public void save(UserRoom object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT)) {
            statement.setLong(1, object.getUserId());
            statement.setLong(2, object.getRoomId());
            int check = statement.executeUpdate();
            if (check > 0) {
                System.out.println("recording to userRoom successfully made");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {

    }

    public void deleteByUserId(Long userId) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_DELETE_BY_USER_ID)) {
            statement.setLong(1, userId);
            int check = statement.executeUpdate();
            if (check > 0) {
                System.out.println("recording has been deleted.");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void update(UserRoom object) {

    }

    @Override
    public UserRoom find(Long id) {
        return null;
    }

    @Override
    public List<UserRoom> findAll() {
        return null;
    }

    public UserRoom findByUserId(Long userId) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_USER_ID)) {
            statement.setLong(1, userId);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return userRoomRowMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
