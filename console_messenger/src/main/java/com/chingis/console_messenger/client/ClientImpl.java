package com.chingis.console_messenger.client;

import java.io.*;
import java.net.Socket;

public class ClientImpl implements Client {
    private Socket socketClient;
    private BufferedReader reader;
    private BufferedWriter writer;
    private ListenServer listenServer;

    //Как правильно закрывать все открытые классы?
    public ClientImpl(String host, int port) {
        try {
            socketClient = new Socket(host, port);
            reader = new BufferedReader(new InputStreamReader(System.in));
            writer = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
            listenServer = new ListenServer();
            listenServer.start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //убрал прерывание треда по слову stop
    @Override
    public void run() {
        String clientMessage;
        try {
            try {
                while (true) {
                    clientMessage = reader.readLine();
                    writer.write(clientMessage + "\n");
                    writer.flush();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            } finally {
                System.out.println("Client is closed...");
                socketClient.close();
                reader.close();
                writer.close();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    void shutdown() {
        Thread.currentThread().interrupt();
    }

    private class ListenServer extends Thread {
        private BufferedReader in;

        ListenServer() {
            try {
                this.in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    String input = in.readLine();
                    if (input.equals("server: confirm exit")) {
                        ClientImpl.this.shutdown();
                        this.interrupt();
                        break;
                    }
                    System.out.println(input);
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
