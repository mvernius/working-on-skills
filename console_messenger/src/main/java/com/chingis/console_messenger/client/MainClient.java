package com.chingis.console_messenger.client;

public class MainClient {
    public static void main(String[] args) {
        Client client = new ClientImpl("127.0.0.1", 7777);
        client.run();
    }
}
