package com.chingis.console_messenger.server.service.interfaces;

import java.util.List;

public interface MessagesService {
    public void messageQuery(Long userId, Long roomId, String message);
    public List<String> showHistory(Long roomId);
    public List<String> showHistory(Long roomId, Integer quantity);
}
