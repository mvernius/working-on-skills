package com.chingis.console_messenger.server.service.utils.command_executors.exit;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.UserState;

/*
Работа exit:
exit room - выход из текущей комнаты
exit - выход из чата (проверка на приход пустой строки)
 */
public class ExitExec extends Command {
    private String commandText;
    public ExitExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        if (client.userState == UserState.JOINED_ROOM) {
            new ExitRoomExec(commandText).execute(client);
        } else {
            new ExitChatExec(commandText).execute(client);
        }
    }
}
