package com.chingis.console_messenger.server.service.utils.command_executors.exit;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;

public class ExitRoomExec extends Command {
    private final String commandText;
    public ExitRoomExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    //знаю что дублируется код
    @Override
    public void execute(ChatSocketClient client) {
        client.migrationService.moveFromUsersToAnonyms();
        client.getServer().userRoomRepository.deleteByUserId(client.getUserId());
        client.userState = UserState.CREATED_USERNAME;
        client.setRoomId(null);
        String message = "You are out of room now.\n" +
                "If you want to continue chatting, please select room.\n" +
                "If you want to see list of available commands run \\?\n" +
                "Be careful, if you repeat exit command your session will end.\n";
        new SystemMessage(ChatSocketClient.ANSI_YELLOW + message).execute(client);
    }
}
