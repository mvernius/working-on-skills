package com.chingis.console_messenger.server.service.utils;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Command implements ClientQuery {
    private String commandText;

    public Command(String commandText) {
        this.commandText = commandText;
    }

    public String getCommandText() {
        return commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {

    }
}
