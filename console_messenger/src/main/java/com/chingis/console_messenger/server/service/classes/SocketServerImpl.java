package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.repositories.*;
import com.chingis.console_messenger.trash.UserRoomRepositoryImpl;
import com.chingis.console_messenger.server.service.interfaces.MessagesService;
import com.chingis.console_messenger.server.service.interfaces.SocketServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class SocketServerImpl implements SocketServer {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/messenger";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "";
    private ServerSocket serverSocket;
    List<ChatSocketClient> anonyms = Collections.synchronizedList(new ArrayList<>());
    Map<Long, List<ChatSocketClient>> users = Collections.synchronizedMap(new HashMap<>());
    private final int port;
    MessagesService messagesService;
    public UserRepositoryImpl userRepository;
    public RoomRepositoryImpl roomRepository;
    public UserRoomRepositoryImpl userRoomRepository;

    public SocketServerImpl(int port) {
        this.port = port;
    }

    @Override
    public void shutdown() throws IOException {
        //TODO: add cycle through users.
        for (Thread thread : anonyms) {
            thread.interrupt();
        }
        if (serverSocket != null) {
            serverSocket.close();
        }
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("server launched");
            connectDataBase();

            while (true) {
                Socket socketClient = serverSocket.accept();
                try {
                    anonyms.add(new ChatSocketClient(this, socketClient));
                } catch (Exception ignored) {
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void connectDataBase() {
        try {
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            userRoomRepository = new UserRoomRepositoryImpl(connection);
            userRepository = new UserRepositoryImpl(connection);
            roomRepository = new RoomRepositoryImpl(connection);
            messagesService = new MessagesServiceImpl(connection);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public MessagesService getMessagesService() {
        return messagesService;
    }

    @Override
    public List<ChatSocketClient> getAnonyms() {
        return anonyms;
    }

    @Override
    public Map<Long, List<ChatSocketClient>> getUsers() {
        return users;
    }
}
