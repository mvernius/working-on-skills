package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.interfaces.CommandController;
import com.chingis.console_messenger.server.service.utils.ClientQuery;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;
import com.chingis.console_messenger.server.service.utils.command_executors.create.CreateExec;
import com.chingis.console_messenger.server.service.utils.command_executors.delete.DeleteExec;
import com.chingis.console_messenger.server.service.utils.command_executors.exit.ExitExec;
import com.chingis.console_messenger.server.service.utils.command_executors.others.InfoCommandExec;
import com.chingis.console_messenger.server.service.utils.command_executors.others.JoinExec;
import com.chingis.console_messenger.server.service.utils.command_executors.others.LoginUserExec;
import com.chingis.console_messenger.server.service.utils.command_executors.others.ShowRoomsExec;

public class CommandControllerImpl implements CommandController {
    private ClientQuery query;
    private UserState userState;

    public CommandControllerImpl(ClientQuery query, UserState userState) {
        this.query = query;
        this.userState = userState;
    }

    private ClientQuery determineCommand(String inputLine) {
        System.out.println("from CommandControllerImpl: " + inputLine);
        if (inputLine.startsWith("create")) {
            return new CreateExec(QueryDefiner.skipSpace(inputLine.substring("create".length())));
        } else if (inputLine.startsWith("join")) {
            return new JoinExec(QueryDefiner.skipSpace(inputLine.substring("join".length())));
        } else if (inputLine.startsWith("delete")) {
            return new DeleteExec(QueryDefiner.skipSpace(inputLine.substring("delete".length())));
        } else if (inputLine.startsWith("exit")) {
            return new ExitExec(QueryDefiner.skipSpace(inputLine.substring("exit".length())));
        } else if (inputLine.startsWith("show")) {
            return new ShowRoomsExec(QueryDefiner.skipSpace(inputLine.substring("show".length())));
        } else if (inputLine.startsWith("login")) {
            return new LoginUserExec(QueryDefiner.skipSpace(inputLine.substring("login".length())));
        } else if (inputLine.startsWith("\\?")) {
            return new InfoCommandExec(inputLine);
        } else {
            return new SystemMessage(ChatSocketClient.ANSI_RED + "Error: wrong command is given.\n" +
                        "Please retry or see command list with \\?\n");
        }
    }

    @Override
    public ClientQuery makeQuery() {
        Command command = (Command)this.query;
//        return determineCommand(command.getCommandText());
        return determineCommand(QueryDefiner.skipSpace(command.getCommandText()));
    }
}
