package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.models.Message;
import com.chingis.console_messenger.server.repositories.MessageRepository;
import com.chingis.console_messenger.server.repositories.MessageRepositoryImpl;
import com.chingis.console_messenger.server.service.interfaces.MessagesService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessagesServiceImpl implements MessagesService {
    private final Connection CONNECTION;
    private MessageRepositoryImpl messageRepository;

    public MessagesServiceImpl(Connection connection) {
        this.CONNECTION = connection;
        this.messageRepository = new MessageRepositoryImpl(this.CONNECTION);
    }

    @Override
    public void messageQuery(Long userId, Long roomId, String message) {
        messageRepository.save(Message.builder()
                .idRoom(roomId)
                .idUser(userId)
                .text(message)
                .build());
    }

    @Override
    public List<String> showHistory(Long roomId) {
        System.out.println("ShowHistory: I'M HERE");
        List<String> result = new ArrayList<>();
        List<Message> messages = messageRepository.findLastN(roomId, 20);
        if (messages != null && !messages.isEmpty()) {
            Collections.reverse(messages);
            for (Message message : messages) {
                result.add(message.getText());
            }
            return result;
        }
        return null;
    }

    @Override
    public List<String> showHistory(Long roomId, Integer quantity) {
        List<String> result = new ArrayList<>();
        List<Message> messages = messageRepository.findLastN(roomId, quantity);
        for (Message message : messages) {
            result.add(message.getText());
        }
        return result;
    }
}
