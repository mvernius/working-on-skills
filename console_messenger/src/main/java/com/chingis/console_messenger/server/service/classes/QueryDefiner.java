package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.utils.*;

public class QueryDefiner {
    private String query;
    private UserState userState;

    public QueryDefiner(String query, UserState userState) {
        this.query = query;
        this.userState = userState;
    }

    private ClientQuery determineQuery(ClientQuery query) {
        if (query instanceof MessageExec) {
            return new MessageControllerImpl(query, userState).makeQuery();
        } else if (query instanceof Command) {
            return new CommandControllerImpl(query, userState).makeQuery();
        }
        return new SystemMessage(ChatSocketClient.ANSI_RED + "Error: Can't define input");
    }

    private ClientQuery determineInputText(String inputLine) {
        System.out.println("From QueryDefiner: " + inputLine);
        if (inputLine.startsWith("command:")) {
            return new Command(inputLine.substring("command:".length()));
        } else if (inputLine.startsWith("\\?")) {
            return new Command(inputLine);
        } else {
            return new MessageExec(inputLine);
        }
    }

    //должен отсюда выйти конечный инстанс
    public ClientQuery define() {
        ClientQuery intermediateValue = determineInputText(skipSpace(this.query));
        return determineQuery(intermediateValue);
    }

    public static String skipSpace(String inputLine) {
        int i = 0;
        if (inputLine == null) {
            return null;
        }
        if (inputLine.isEmpty())
            return inputLine;
        while (inputLine.charAt(i) == ' ') {
            i++;
        }
        return inputLine.substring(i);
    }
}
