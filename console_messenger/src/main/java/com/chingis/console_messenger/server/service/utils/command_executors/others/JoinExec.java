package com.chingis.console_messenger.server.service.utils.command_executors.others;

import com.chingis.console_messenger.server.models.Room;
import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.classes.SocketServerImpl;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.MessageExec;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;
import com.chingis.console_messenger.trash.UserRoom;

import java.util.List;

public class JoinExec extends Command {
    private String commandText;
    SocketServerImpl server;
    ChatSocketClient client;
    SystemMessage systemMessage;
    public JoinExec(String commandText) {
        this.commandText = commandText;
        this.systemMessage = new SystemMessage("");
    }

    @Override
    public void execute(ChatSocketClient client) {
        this.client = client;
        this.server = client.getServer();
        if (client.userState == UserState.EMPTY) {
            systemMessage.executeMessage(client, ChatSocketClient.ANSI_RED + "Error: Create username first.");
        } else {
            if (client.userState == UserState.CREATED_USERNAME) {
                joinRoom(client);
            } else {
                client.migrationService.moveFromUsersToAnonyms();
                server.userRoomRepository.deleteByUserId(client.getUserId());
                client.userState = UserState.CREATED_USERNAME;
                client.setRoomId(null);
                boolean flag = joinRoom(client);
                if (!flag) {
                    systemMessage.executeMessage(client, ChatSocketClient.ANSI_YELLOW + "You are out of room now. Please select room.");
                }
            }
        }
    }
    private boolean joinRoom(ChatSocketClient client) {
        Room room = server.roomRepository.findByRoomName(commandText);
        if (room != null) {
            client.setRoomId(room.getId());
            client.migrationService.moveFromAnonymsToUsers();
            UserRoom userRoom = new UserRoom(client.getUserId(), room.getId());
            if (server.userRoomRepository.findByUserId(client.getUserId()) == null) {
                server.userRoomRepository.save(userRoom);
            }
            client.userState = UserState.JOINED_ROOM;
            systemMessage.executeMessage(client, ChatSocketClient.ANSI_YELLOW + "Successfully joined room " + commandText);
            showLastMessages(server.getMessagesService().showHistory(room.getId()));
            new MessageExec(ChatSocketClient.ANSI_YELLOW + client.username + " joined the room.").notifyNeighbours(client);
            return true;
        } else {
            systemMessage.executeMessage(client, ChatSocketClient.ANSI_RED + "Error: Room does not exist.");
            return false;
        }
    }

    private void showLastMessages(List<String> messages) {
        if (messages != null) {
            for (String message : messages) {
                systemMessage.executeMessage(this.client, message);
            }
        } else {
            systemMessage.executeMessage(this.client, "Empty history chat");
        }
    }
}
