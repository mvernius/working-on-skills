package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.Room;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoomRepositoryImpl implements RoomRepository {
    private final Connection CONNECTION;

    //language=SQL
    private final static String SQL_FIND_BY_ID = "select * from app_room where id = (?)";
    //language=SQL
    private final static String SQL_FIND_ALL = "select * from app_room";
    //language=SQL
    private final static String SQL_INSERT = "insert into app_room (room_name) values (?)";
    //language=SQL
    private static final String SQL_FIND_BY_ROOM_NAME = "select * from app_room where room_name = (?)";

    private final RowMapper<Room> roomRowMapper = row -> new Room(
            row.getLong("id"),
            row.getString("room_name")
    );

    public RoomRepositoryImpl(Connection connection) {
        this.CONNECTION = connection;
    }



    @Override
    public void save(Room object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT)) {
            statement.setString(1, object.getRoomName());
            int check = statement.executeUpdate();
            if (check > 0) {
                System.out.println("Room was successfully created");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public void update(Room object) {

    }

    // noinspection DuplicatedCode
    @Override
    public Room find(Long id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setLong(1, id);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                resultSet.next();
                return roomRowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Room> findAll() {
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<Room> returnList = new ArrayList<>();
            while (resultSet.next()) {
                returnList.add(roomRowMapper.mapRow(resultSet));
            }
            return returnList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Room findByRoomName(String roomName) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ROOM_NAME)) {
            statement.setString(1, roomName);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return roomRowMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
