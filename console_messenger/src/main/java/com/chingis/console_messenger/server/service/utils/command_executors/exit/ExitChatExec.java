package com.chingis.console_messenger.server.service.utils.command_executors.exit;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;

import java.util.List;

public class ExitChatExec extends Command {
    private final String commandText;
    public ExitChatExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        List<ChatSocketClient> anonyms = client.getServer().getAnonyms();
        for (int i = 0; i < anonyms.size(); i++) {
            if (client.equals(anonyms.get(i))) {
                anonyms.remove(i);
                break;
            }
        }
        new SystemMessage("server: confirm exit").execute(client);
        client.interrupt();
    }
}
