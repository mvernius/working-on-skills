package com.chingis.console_messenger.server.service.utils.command_executors.others;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;

public class InfoCommandExec extends Command {
    public static int counter;

    static {
        counter = 0;
    }
    private String commandText;
    public InfoCommandExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        String info;
        StringBuilder builder = new StringBuilder();
        if (counter == 0) {
            builder.append("Hello, Dear User!\n").append("This is simple multi-client console Messenger.\n");
        }
        builder.append("--------------------------USER GUIDE--------------------------\n");
        builder.append("If you want to ask server run some command, you should start your prompt with <command:>\n");
        builder.append("(now and further <> are for example and should be ignored during usage process)\n");
        builder.append("Any other prompted text will be interpreted as message.\n");
        builder.append("-------------------------COMMAND LIST-------------------------\n");
        builder.append("command: create user <username> | let's you to create your username\n");
        builder.append("command: create room <roomname> | let's you to create custom room\n");
        builder.append("command: login <username>       | restores your last session\n");
        builder.append("command: show rooms             | shows you available rooms to join\n");
        builder.append("command: join <roomname>        | let's you to join the room\n");
        builder.append("command: exit                   | 1. Exits the room if you are in\n");
        builder.append("                                | 2. Exits the app if you are not in the room\n");
        builder.append("\\?                              | for info\n");
        info = builder.toString();
        new SystemMessage(ChatSocketClient.ANSI_YELLOW + info).execute(client);
        counter++;
    }
}
