package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.User;

public interface UserRepository extends CrudRepository<User> {
}
