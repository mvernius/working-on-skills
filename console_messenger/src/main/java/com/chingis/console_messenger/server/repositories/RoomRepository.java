package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.Room;

public interface RoomRepository extends CrudRepository<Room> {
}
