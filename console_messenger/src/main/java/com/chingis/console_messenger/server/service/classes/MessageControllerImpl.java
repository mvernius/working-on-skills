package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.interfaces.MessageController;
import com.chingis.console_messenger.server.service.utils.ClientQuery;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.MessageExec;
import com.chingis.console_messenger.server.service.utils.UserState;

public class MessageControllerImpl implements MessageController {
    private final ClientQuery query;
    UserState userState;

    public MessageControllerImpl(ClientQuery query, UserState userState) {
        this.query = query;
        this.userState = userState;
    }

    private ClientQuery messageControl(ClientQuery clientQuery) {
        if (userState != UserState.JOINED_ROOM) {
            String errorMessage;
            StringBuilder builder = new StringBuilder();
            builder.append("Error: Can't write message.\n");
            if (this.userState == UserState.EMPTY) {
                builder.append("Create username first.\n");
                errorMessage = builder.append("Then join to the room.\n").toString();
            } else {
                errorMessage = builder.append("Join to the room.\n").toString();
            }
            return new SystemMessage(ChatSocketClient.ANSI_RED + errorMessage);
        } else {
            MessageExec messageExec = (MessageExec)clientQuery;
            return new MessageExec(ChatSocketClient.ANSI_BLUE + messageExec.getMessage());
        }
    }

    @Override
    public ClientQuery makeQuery() {
        return messageControl(this.query);
    }
}
