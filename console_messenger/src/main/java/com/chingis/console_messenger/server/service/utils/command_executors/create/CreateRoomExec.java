package com.chingis.console_messenger.server.service.utils.command_executors.create;

import com.chingis.console_messenger.server.models.Room;
import com.chingis.console_messenger.server.repositories.RoomRepositoryImpl;
import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;

public class CreateRoomExec extends Command {
    private String commandText;
    private static Long roomCounter;

    static {
        roomCounter = 2L;
    }

    public CreateRoomExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        if (client.userState == UserState.CREATED_USERNAME) {
            RoomRepositoryImpl roomRep = client.getServer().roomRepository;
            if (roomRep.findByRoomName(commandText) == null) {
                Room room = Room.builder()
                        .id(roomCounter)
                        .roomName(commandText)
                        .build();
                roomRep.save(room);
                roomCounter++;
                new SystemMessage(ChatSocketClient.ANSI_YELLOW + "Successfully created room" +
                        "<" + commandText + ">").execute(client);
            }
        } else {
            if (client.userState == UserState.EMPTY) {
                new SystemMessage(ChatSocketClient.ANSI_RED + "Create username first\n").execute(client);
            } else {
                new SystemMessage(ChatSocketClient.ANSI_RED + "Leave the room, and then try to create the new one.").execute(client);
            }
        }
    }

//        if (client.userState == UserState.CREATED_USERNAME) {
//            if (client.server.roomRepository.findByRoomName(input[2]) == null) {
//                Room room = Room.builder()
//                        .id(roomCounter)
//                        .roomName(input[2])
//                        .build();
//                client.server.roomRepository.save(room);
//
//                roomCounter++;
//            } else {
//                return "Room with the same name already exists.";
//            }
//        } else {
//            if (client.userState == UserState.EMPTY)
//                return "Create username first.";
//            else
//                return "leave the room, and after try to create one";
//        }
//
}
