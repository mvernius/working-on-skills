package com.chingis.console_messenger.server.service.utils;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;

import java.io.PrintWriter;

public class SystemMessage implements ClientQuery {
    private final String message;

    public SystemMessage(String message) {
        this.message = message;
    }

    @Override
    public void execute(ChatSocketClient client) {
        client.getOutcome().println(message);
    }

    public void executeMessage(ChatSocketClient client, String text) {
        client.getOutcome().println(text);
    }

    //TODO: доработать
    public void executeBeaty(ChatSocketClient client) {
        PrintWriter outcome = client.getOutcome();
        try {
            for (int i = 0; i < message.length(); i++) {
                outcome.print(message.charAt(i));
                outcome.println("!");
                Thread.sleep(1);
            }
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
