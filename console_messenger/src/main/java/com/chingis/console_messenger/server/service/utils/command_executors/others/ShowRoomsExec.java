package com.chingis.console_messenger.server.service.utils.command_executors.others;

import com.chingis.console_messenger.server.models.Room;
import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;

import java.util.List;

public class ShowRoomsExec extends Command {
    private final String commandText;
    public ShowRoomsExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        if (commandText.equalsIgnoreCase("rooms")) {
            List<Room> roomList = client.getServer().roomRepository.findAll();
            new SystemMessage(ChatSocketClient.ANSI_YELLOW + "----AVAILABLE ROOMS----").execute(client);
            for (Room room : roomList) {
                new SystemMessage(ChatSocketClient.ANSI_YELLOW + room.getRoomName()).execute(client);
            }
        }
    }
}
