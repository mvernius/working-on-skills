package com.chingis.console_messenger.server.repositories;

import java.util.List;

public interface CrudRepository<T> {
    void save(T object);
    void delete(Integer id);
    void update(T object);
    T find(Long id);
    List<T> findAll();
}
