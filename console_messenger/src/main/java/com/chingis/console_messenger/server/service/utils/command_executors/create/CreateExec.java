package com.chingis.console_messenger.server.service.utils.command_executors.create;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.classes.QueryDefiner;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;

import java.util.Locale;

public class CreateExec extends Command {
    private String command;

    public CreateExec(String command) {
        this.command = command;
    }

    @Override
    public void execute(ChatSocketClient client) {
        System.out.println("From CreateExec: " + command);
        if (command.startsWith("user")) {
            new CreateUserExec(QueryDefiner.skipSpace(command.substring(4))).execute(client);
        } else if (command.startsWith("room")) {
            new CreateRoomExec(QueryDefiner.skipSpace(command.substring(4))).execute(client);
        } else {
            new SystemMessage(ChatSocketClient.ANSI_RED + "Wrong creation").execute(client);
        }
    }
}
