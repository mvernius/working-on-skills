package com.chingis.console_messenger.server.service.utils;

public enum UserState {
    EMPTY,
    CREATED_USERNAME,
    JOINED_ROOM
}
