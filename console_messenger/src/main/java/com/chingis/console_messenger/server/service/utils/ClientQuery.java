package com.chingis.console_messenger.server.service.utils;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;

public interface ClientQuery {
    void execute(ChatSocketClient client);
}
