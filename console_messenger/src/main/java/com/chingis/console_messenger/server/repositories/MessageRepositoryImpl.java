package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryImpl implements MessageRepository {
    private final Connection CONNECTION;

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from message where id = (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from message";
    //language=SQL
    private static final String SQL_INSERT = "insert into message (sentence, id_user, id_room) values (?, ?, ?)";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from message where id = (?)";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update message set sentence = (?) where id = (?)";
    //language=SQL
    private static final String SQL_FIND_LAST_N = "select * from message where id_room = (?) order by id desc limit (?)";

    public MessageRepositoryImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private final RowMapper<Message> messageRowMapper = row -> new Message(
            row.getLong("id"),
            row.getString("sentence"),
//            row.getLong("id_user_room")
            row.getLong("id_user"),
            row.getLong("id_room")
    );

    @Override
    public void save(Message object) {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SQL_INSERT)) {
            preparedStatement.setString(1, object.getText());
            preparedStatement.setLong(2, object.getIdUser());
            preparedStatement.setLong(3, object.getIdRoom());
            int check = preparedStatement.executeUpdate();
            if (check > 0)
                System.out.println("Message was saved");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public void update(Message object) {

    }

    @Override
    public Message find(Long id) {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SQL_FIND_BY_ID)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                resultSet.next();
                return messageRowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Message> findAll() {
        try (Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<Message> returnList = new ArrayList<>();
            while (resultSet.next()) {
                returnList.add(messageRowMapper.mapRow(resultSet));
            }
            return returnList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Message> findLastN(Long roomId, Integer quantity) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_LAST_N)) {
            statement.setLong(1, roomId);
            statement.setInt(2, quantity);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                List<Message> list = new ArrayList<>();
                while (resultSet.next()) {
                    list.add(messageRowMapper.mapRow(resultSet));
                }
                return list;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
