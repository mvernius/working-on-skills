package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.Message;

public interface MessageRepository extends CrudRepository<Message> {
}
