package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.interfaces.CommandController;
import com.chingis.console_messenger.server.service.interfaces.MigrationService;
import com.chingis.console_messenger.server.service.utils.ClientQuery;
import com.chingis.console_messenger.server.service.utils.UserState;
import com.chingis.console_messenger.server.service.utils.command_executors.others.InfoCommandExec;

import java.io.*;
import java.net.Socket;


/*
это и есть клиент
*/

public class ChatSocketClient extends Thread{
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    final SocketServerImpl server;
    private final BufferedReader income;
    private final PrintWriter outcome;
    private Long userId;
    private Long roomId;
    public String username;
    public MigrationService migrationService;
    public UserState userState;


    public ChatSocketClient(SocketServerImpl server, Socket client) throws IOException {
        this.server = server;
        this.income = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.outcome = new PrintWriter(client.getOutputStream(), true);
        this.migrationService = new MigrationServiceImpl(this.server, this);
        this.userState = UserState.EMPTY;
        start();
    }

    public SocketServerImpl getServer() { return this.server; }
    public PrintWriter getOutcome() {
        return outcome;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getRoomId() {
        return roomId;
    }

    @Override
    public void run() {
        try {
            new InfoCommandExec("").execute(this);
            while (true) {
                String inputLine = this.income.readLine();
                QueryDefiner queryDefiner = new QueryDefiner(inputLine, this.userState);
                ClientQuery clientQuery = queryDefiner.define();
                clientQuery.execute(this);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
