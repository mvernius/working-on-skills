package com.chingis.console_messenger.server.service.interfaces;

public interface MigrationService {
    public void doMigration();
    public void moveFromAnonymsToUsers();
    public void moveFromUsersToAnonyms();
}
