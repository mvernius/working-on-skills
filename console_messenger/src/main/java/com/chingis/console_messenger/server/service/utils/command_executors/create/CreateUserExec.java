package com.chingis.console_messenger.server.service.utils.command_executors.create;

import com.chingis.console_messenger.server.models.User;
import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.classes.SocketServerImpl;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;

public class CreateUserExec extends Command {
    private String commandText;
    private static Long userCounter;

    static {
        userCounter = 2L;
    }
    public CreateUserExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return this.commandText;
    }

    @Override
    public void execute(ChatSocketClient client) {
        if (client.userState == UserState.EMPTY) {
            SocketServerImpl server = client.getServer();
            if (server.userRepository.findByUsername(commandText) == null) {
                User user = User.builder()
                        .id(userCounter)
                        .userName(commandText)
                        .build();
                server.userRepository.save(user);
                client.setUserId(server.userRepository.findByUsername(commandText).getId());
                client.userState = UserState.CREATED_USERNAME;
                client.username = commandText;
                userCounter++;
                new SystemMessage(ChatSocketClient.ANSI_YELLOW + "Successfully created.\n" +
                        "Now your username is " + commandText + "\n").execute(client);
            } else {
                new SystemMessage(ChatSocketClient.ANSI_RED + "Error: User already exists. Try login or change nickname.").execute(client);
            }
        } else {
            new SystemMessage(ChatSocketClient.ANSI_RED + "You already have a username\n").execute(client);
        }
    }
}
