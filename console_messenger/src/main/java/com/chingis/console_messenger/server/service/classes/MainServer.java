package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.interfaces.SocketServer;

public class MainServer {
    public static void main(String[] args) {
        int port = 7777;
        SocketServer server = new SocketServerImpl(port);
        server.run();
    }
}
