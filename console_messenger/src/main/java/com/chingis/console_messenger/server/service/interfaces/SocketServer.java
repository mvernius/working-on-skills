package com.chingis.console_messenger.server.service.interfaces;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface SocketServer extends Runnable {
    void shutdown() throws IOException;
    List<ChatSocketClient> getAnonyms();
    Map<Long, List<ChatSocketClient>> getUsers();
}
