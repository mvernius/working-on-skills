package com.chingis.console_messenger.server.repositories;

import com.chingis.console_messenger.server.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    private final Connection CONNECTION;

    //language=SQL
    private static final String SQL_INSERT = "insert into app_user (nickname) values (?)";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from app_user where id = (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from app_user";
    //language=SQL
    private static final String SQL_FIND_BY_USERNAME = "select * from app_user where nickname = (?)";

    public UserRepositoryImpl(Connection connection) {
        this.CONNECTION = connection;
    }

    private final RowMapper<User> userRowMapper = row -> new User(
            row.getLong("id"),
            row.getString("nickname")
    );

    @Override
    public void save(User object) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_INSERT)) {
            statement.setString(1, object.getUserName());
            int check = statement.executeUpdate();
            if (check > 0) {
                System.out.println("User successfully added");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public void update(User object) {

    }

    @Override
    public User find(Long id) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setLong(1, id);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                resultSet.next();
                return userRowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findAll() {
        try (Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {
            List<User> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(userRowMapper.mapRow(resultSet));
            }
            return list;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //можно default метод у абстрактного класса в таких случаях делать, только хз, как строку запроса подменить
    public User findByUsername(String username) {
        try (PreparedStatement statement = CONNECTION.prepareStatement(SQL_FIND_BY_USERNAME)) {
            statement.setString(1, username);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    return userRowMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
