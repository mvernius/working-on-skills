package com.chingis.console_messenger.server.service.interfaces;

import com.chingis.console_messenger.server.service.utils.ClientQuery;

public interface QueryController {
    public ClientQuery makeQuery();
}
