package com.chingis.console_messenger.server.service.classes;

import com.chingis.console_messenger.server.service.interfaces.MigrationService;
import com.chingis.console_messenger.server.service.interfaces.SocketServer;

import java.util.*;

public class MigrationServiceImpl implements MigrationService {
    SocketServer server;
    ChatSocketClient client;
    List<ChatSocketClient> anonyms;
    Map<Long, List<ChatSocketClient>> users;


    public MigrationServiceImpl(SocketServer server, ChatSocketClient client) {
        this.server = server;
        this.client = client;
        this.anonyms = server.getAnonyms();
        this.users = server.getUsers();
    }

    //FIXME: does equals work fine? Which method is better?
    @Override
    public void moveFromAnonymsToUsers() {
        for (int i = 0; i < anonyms.size(); i++) {
            if (anonyms.get(i).equals(client)) {
                anonyms.remove(i);
                break;
            }
        }
        users.computeIfAbsent(client.getRoomId(), k -> new ArrayList<>());
        users.get(client.getRoomId()).add(client);
//        anonyms.remove(client);
    }

    @Override
    public void moveFromUsersToAnonyms() {
        users.get(client.getRoomId()).remove(client);
        anonyms.add(client);
    }

    @Override
    public void doMigration() {

    }

}
