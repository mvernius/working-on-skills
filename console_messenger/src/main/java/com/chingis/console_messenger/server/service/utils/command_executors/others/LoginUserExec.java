package com.chingis.console_messenger.server.service.utils.command_executors.others;

import com.chingis.console_messenger.server.models.User;
import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.classes.SocketServerImpl;
import com.chingis.console_messenger.server.service.utils.Command;
import com.chingis.console_messenger.server.service.utils.SystemMessage;
import com.chingis.console_messenger.server.service.utils.UserState;
import com.chingis.console_messenger.trash.UserRoom;

import java.io.PrintWriter;
import java.util.List;

public class LoginUserExec extends Command {
    private String commandText;
    public LoginUserExec(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public String getCommandText() {
        return super.getCommandText();
    }

    @Override
    public void execute(ChatSocketClient client) {
        SocketServerImpl server = client.getServer();
        User user = server.userRepository.findByUsername(commandText);
        if (user == null) {
            new SystemMessage(ChatSocketClient.ANSI_RED + "Error: can't find username").execute(client);
        } else {
            client.setUserId(user.getId());
            client.userState = UserState.CREATED_USERNAME;
            client.username = commandText;
            UserRoom userRoom = client.getServer().userRoomRepository.findByUserId(user.getId());
            new SystemMessage(ChatSocketClient.ANSI_YELLOW + "Successful login\n").execute(client);
            if (userRoom == null) {
                StringBuilder builder = new StringBuilder();
                builder.append("Hello, ").append(commandText).append("\n");
                builder.append("It seems like you are not in room now.\n");
                builder.append("Please enter the room to start chat.\n");
                String message = builder.toString();
                new SystemMessage(ChatSocketClient.ANSI_YELLOW + message).execute(client);
            } else {
                String roomName = client.getServer().roomRepository.find(userRoom.getRoomId()).getRoomName();
                new JoinExec(roomName).execute(client);
            }
        }
    }
}
