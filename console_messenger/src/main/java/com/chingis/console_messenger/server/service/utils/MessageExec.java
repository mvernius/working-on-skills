package com.chingis.console_messenger.server.service.utils;

import com.chingis.console_messenger.server.service.classes.ChatSocketClient;
import com.chingis.console_messenger.server.service.classes.SocketServerImpl;
import com.chingis.console_messenger.server.service.interfaces.MessagesService;

import java.util.List;

public class MessageExec implements ClientQuery {
    private final String message;
    private SocketServerImpl server;
    private MessagesService messagesService;

    public MessageExec(String message) {
        this.message = message;
    }

    @Override
    public void execute(ChatSocketClient client) {
        this.server = client.getServer();
        this.messagesService = server.getMessagesService();
        String text = server.userRepository.find(client.getUserId()).getUserName() + ": " + message;
        messagesService.messageQuery(client.getUserId(), client.getRoomId(), text);
        List<ChatSocketClient> recipients = server.getUsers().get(client.getRoomId());
        if (recipients != null && !recipients.isEmpty()) {
            for (ChatSocketClient toClient : recipients) {
                if (toClient != client) {
                    toClient.getOutcome().println(ChatSocketClient.ANSI_GREEN + text);
                }
            }
        }
    }

    public void notifyNeighbours(ChatSocketClient client) {
        this.server = client.getServer();
        List<ChatSocketClient> recipients = server.getUsers().get(client.getRoomId());
        if (recipients != null && !recipients.isEmpty()) {
            for (ChatSocketClient toClient : recipients) {
                if (toClient != client) {
                    toClient.getOutcome().println(message);
                }
            }
        }
    }

    public String getMessage() {
        return message;
    }
}
