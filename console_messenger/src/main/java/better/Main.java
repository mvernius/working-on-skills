package better;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class Main {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setUsername("");
        config.setPassword("");
        config.setJdbcUrl("");
        config.setMaximumPoolSize(20);
        config.setDriverClassName("org.postgresql.Driver");
        HikariDataSource dataSource = new HikariDataSource(config);
        ProductRepository productRepository = new ProductRepositoryJdbcImpl(dataSource);
    }
}
