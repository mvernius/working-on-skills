package better;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public class ProductRepositoryJdbcImpl implements ProductRepository {

    private DataSource dataSource;

    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    @Override
    public List<Product> findAll() {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * from product where id = ?")) {
            // ...
            statement.setInt(1, 10);
            try (ResultSet result = statement.executeQuery()) {
                // process
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }
}
