package better;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
}
