module.exports = function(app) {
    app.get('/users'), (request, response) => {
        var result = [
            {
                "id": 1,
                "name": "Marsel"
            },
            {
                "id": 2,
                "name": "Timur"
            },
            {
                "id": 3,
                "name": "Archibald"
            }
        ];
        response.send(JSON.stringify(result));
    }
}