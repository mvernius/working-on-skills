// используем библиотеку экспресс
const express = require('express');
// const bodyParser = require('body-parser');
// создаем объект экспресс
const app = express();
// говорим, что мы раздаем папку public 
app.use(express.static('public'));
// app.use(bodyParser.urlencoded({ extended: true }));
// require('./public/scripts')(app);
// говорим, что запускаем на 80 порту.
app.listen(80);
console.log("server started at 80");