package com.chingis.reverse;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReverseTest {

    @Test
    public void testWithRightParam() {
        assertEquals("olleH dlrow", Reverse.reverseWithSameWordsOrder("Hello world"));
        assertEquals("dlrow olleH", Reverse.reverseFullString("Hello world"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullParam() {
        Reverse.reverseFullString(null);
        Reverse.reverseWithSameWordsOrder(null);
    }
}