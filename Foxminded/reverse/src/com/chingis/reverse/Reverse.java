package com.chingis.reverse;


public class Reverse implements Bird, Fish {

    public static void main(String[] args) {
//        System.out.println(reverseWithSameWordsOrder("Hello world"));
//        System.out.println(reverseFullString("Hello world"));
//        Reverse rv = new Reverse();
//        rv.fooBird();
//        rv.fooFish();
//        rv.foo();
        String word;
        StringBuilder builder = new StringBuilder();
        builder.append("h");
        builder.append("e");
        word = builder.append("ll").toString();
        System.out.println(word);
    }

    public void fooBird() {
        Bird.super.foo();
    }

    public void fooFish() {
        Fish.super.foo();
    }

    @Override
    public void foo() {
        System.out.println("new foo");
    }

    public static String reverseWithSameWordsOrder(String input) {
        try {
            String[] inputWords = input.split(" ");
            String[] outputWords = new String[inputWords.length];
            int i = 0;
            for (String word : inputWords) {
                outputWords[i] = new StringBuilder(word).reverse().toString();
                i++;
            }
            return String.join(" ", outputWords);
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String reverseFullString(String input) {
        return new StringBuilder(input).reverse().toString();
    }
}

